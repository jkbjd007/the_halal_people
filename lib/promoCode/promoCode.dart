import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
class promoCode extends DrawerContent {
  @override
  _promoCodeState createState() => _promoCodeState();
}

class _promoCodeState extends State<promoCode> {
  bool radio1 = true;
  bool radio2 = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
          trailingIcon: InkWell(
              onTap: widget.onMenuPressed,
              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Profile", style: styles.pageTitleStyle(),),
          leadingIcon: Image.asset("assets/icons/rightArrowIcon.png"),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                children: <Widget>[
                  RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              children: [
                                TextSpan(
                                  text:"My Promocodes",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),
                                )
                              ]
                          ),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 5),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text:"Drive license number is needed if driver has registered a car. For bicycle it is not necessary."
                              ,style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 48)),
              child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("XDW34DFG",style: styles.fontRegular(fontSize: size.convert(context, 16),color: Colors.grey),),
                Text("02.05.2019",style: styles.fontRegular(fontSize: size.convert(context, 16),color: Colors.grey),)
              ],
            ),),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 48)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("XDW34DFG",style: styles.fontRegular(fontSize: size.convert(context, 16)),),
                  Text("02.05.2019",style: styles.fontRegular(fontSize: size.convert(context, 16)),)
                ],
              ),),
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                children: <Widget>[
                  Text("Add a New Promocode",
                    style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              child: customTextField(
                textFieldWidth: size.convertWidth(context, 331),
                textFieldHeight: size.convert(context, 58),
                isPadding: true,
                hints: "",
                radius: 10,
              ),
            ),
            SizedBox(height: size.convert(context, 15),),
            swapButton(
              onClick: (){
                Navigator.pop(context);
              },
              buttonWidth: size.convertWidth(context, 331),
              buttonHieght: size.convert(context, 55),
              textColor: Colors.white,
              buttonText: "SAVE",
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              buttonColor: buttonColor,
              borderColor: Colors.transparent,
            ),

          ],),),);
  }
}
