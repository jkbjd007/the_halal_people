
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/messanger/messenger.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/changePassword.dart';
import 'package:the_halal_people/profile/personalnfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';

class notification extends StatefulWidget {

  @override
  _notificationState createState() => _notificationState();
}

class _notificationState extends State<notification> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/message.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Notifications",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Lorem ipsum dolor sit amet, consectetur non adipiscing elit. Etiam ac tempor leo.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    //Navigator.push(context, PageTransition(child: addCompanyInfo(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Icon(IcoFontIcons.notification,
                  color: Colors.grey,),
                  buttonText: "Notifications",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
            SizedBox(height: size.convert(context, 10),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: messenger(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Icon(IcoFontIcons.facebookMessenger,
                    color: Colors.grey,),
                  buttonText: "Messenger",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
          ],),
      ),
    );
  }
}
