
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';

class messenger extends StatefulWidget {
  @override
  _messengerState createState() => _messengerState();
}

class _messengerState extends State<messenger> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  bool isStockAvilable = true;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Chat Online",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: Stack(
        children: <Widget>[
          MessageBody(),
          Align(
            alignment: Alignment.bottomCenter,
            child: bottomBar(),
          ),
        ],
      ),
    );
  }
  bottomBar(){
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
        border: Border(
          top: BorderSide(
            width: 0.5,
            color: Colors.grey
          )
        )
      ),
      padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20),
      vertical: size.convert(context, 10)),
      child: Row(
        children: <Widget>[
          Icon(IcoFontIcons.plus,
          size: size.convert(context, 15),),
          SizedBox(width: 17),
          InkWell(
            onTap: (){
              _settingModalBottomSheet();
            },
            child: Icon(IcoFontIcons.camera,
              size: size.convert(context, 18),
              color: Colors.black.withOpacity(0.5),
            ),
          ),
          SizedBox(width: 17),
          Container(
            width: size.convertWidth(context, 230),
            height: size.convert(context, 44),
            decoration: BoxDecoration(
              border:Border.all(
                width: 0.5,
                color: Color(0xffd3e0f0)
              )
            ),
            child: TextField(
              maxLines: 10,
//              enabled: isEnable,
//              obscureText: obscureText ?? false,
//              keyboardType: textInputType == null  ? TextInputType.text: textInputType,
//              controller: textEditingController,
              style: TextStyle(
                  fontSize:  16,
                  fontFamily: "RobotoLight",
                  color: Colors.black
              ),
              decoration: InputDecoration(
                disabledBorder: InputBorder.none,
                border: InputBorder.none,
                hintText:  "",
                hintStyle: styles.hintsStyle(),

              ),
            ),
          ),
          SizedBox(width: 40),
          Icon(Icons.mic,
            size: size.convert(context, 25),
            color: Colors.black.withOpacity(0.5),
          )
        ],
      ),

    );
  }
  MessageBody(){
    return Container(
      padding: EdgeInsets.only(bottom: size.convert(context, 40)),
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 37)),
      child: SingleChildScrollView(
        child:Column(children: <Widget>[
          messageOut("Can you take a picture of your location ?"),
          messageOut("I am waiting for you now"),
          messageIn("Can you take a picture of your location ?"),
          messageIn("Can you take a picture of your location ?"),
          messageOut("Can you take a picture of your location ?"),
          messageOut("Can you take a picture of your location ?"),
          messageOut("Can you take a picture of your location ?"),
        ],),
      ),
    );
  }
  messageIn(String text1){
    return Container(
      margin: EdgeInsets.symmetric(vertical: size.convert(context, 10)),
      child: Row(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              Align(
                child: Container(
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xff4cd964)
                  ),
                ),
              ),
              Container(
                width: size.convert(context, 32),
                height: size.convert(context, 32),
                child: circularImage(
                assetImage: true,
                imageUrl: "assets/icons/imMessage.png",
                w: size.convert(context, 32),
                h: size.convert(context, 32),
              ),),
            ],
          ),
          SizedBox(width: size.convertWidth(context, 10),),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(
                bottom: size.convert(context, 36)
              ),
              padding: EdgeInsets.symmetric(vertical: size.convert(context, 7),
              horizontal: size.convertWidth(context, 15)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(size.convert(context, 10)),
                  topRight: Radius.circular(size.convert(context, 10)),
                  bottomRight: Radius.circular(size.convert(context, 10)),

                ),
                color: buttonColor
              ),
              child: Text(text1??"",
              style: styles.fontRegular(fontSize: size.convert(context, 16),
              color: Colors.white),),),
          ),
          SizedBox(width: size.convertWidth(context, 10),),
          Container(
              margin: EdgeInsets.only(
                  bottom: size.convert(context, 26)
              ),
            width: size.convertWidth(context, 60),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text("Oct.16",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 10)),
                    ),
                    SizedBox(width: size.convert(context, 3),),
                        SvgPicture.asset("assets/bottomMenu/scheduleDis.svg",
                        width: 10,
                        height: 10,)

                  ],
                ),
                SizedBox(height: 3,),
                Row(
                  children: <Widget>[
                    Text("17:37 PM",
                      style: styles.pageTitleStyle(fontSize: size.convert(context, 10)),
                    ),
                    SizedBox(width: size.convert(context, 3),),
                      Icon(Icons.access_time,
                      size: 10,color: Colors.grey,)
//                    SvgPicture.asset("assets/bottomMenu/scheduleDis.svg",
//                      width: 10,
//                      height: 10,)

                  ],
                ),
              ],
            )),
        ],
      ),
    );
  }
  messageOut(String text1){
    return Container(
      margin: EdgeInsets.symmetric(vertical: size.convert(context, 10)),
      child: Row(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(
                  bottom: size.convert(context, 26)
              ),
              width: size.convertWidth(context, 60),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      SvgPicture.asset("assets/bottomMenu/scheduleDis.svg",
                        width: 10,
                        height: 10,),
                      SizedBox(width: size.convert(context, 3),),
                      Text("Oct.16",
                        style: styles.pageTitleStyle(fontSize: size.convert(context, 10)),
                      ),
                    ],
                  ),
                  SizedBox(height: 3,),
                  Row(
                    children: <Widget>[
                      Icon(Icons.access_time,
                        size: 10,color: Colors.grey,),
                      SizedBox(width: size.convert(context, 3),),
                      Text("17:37 PM",
                        style: styles.pageTitleStyle(fontSize: size.convert(context, 10)),
                      ),

//                    SvgPicture.asset("assets/bottomMenu/scheduleDis.svg",
//                      width: 10,
//                      height: 10,)

                    ],
                  ),
                ],
              )),
          SizedBox(width: size.convertWidth(context, 10),),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(
                  bottom: size.convert(context, 36)
              ),
              padding: EdgeInsets.symmetric(vertical: size.convert(context, 7),
                  horizontal: size.convertWidth(context, 15)),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(size.convert(context, 10)),
                    topRight: Radius.circular(size.convert(context, 10)),
                    bottomLeft: Radius.circular(size.convert(context, 10)),

                  ),
                  color: Color(0xff3b5998)
              ),
              child: Text(text1??"",
                style: styles.fontRegular(fontSize: size.convert(context, 16),
                    color: Colors.white),),),
          ),
          SizedBox(width: size.convertWidth(context, 10),),
          Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              Align(
                child: Container(
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xff4cd964)
                  ),
                ),
              ),
              Container(
                width: size.convert(context, 32),
                height: size.convert(context, 32),
                child: circularImage(
                  assetImage: true,
                  imageUrl: "assets/icons/imMessage.png",
                  w: size.convert(context, 32),
                  h: size.convert(context, 32),
                ),),
            ],
          ),
        ],
      ),
    );
  }


  _settingModalBottomSheet() {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: size.convert(context, 90),
            color: Colors.white,
            padding: EdgeInsets.symmetric(
                horizontal: size.convert(context, 22),
                vertical: size.convert(context, 16)),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    print("Take a Photo");
                    _onImageButtonPressed(ImageSource.camera);
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: size.convert(context, 10)),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "take A Photo",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    print("Upload from gallery");
                    Navigator.pop(context);
                    _onImageButtonPressed(ImageSource.gallery);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: size.convert(context, 10),
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "Upload From Gallery",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
  _onImageButtonPressed(ImageSource sourceFile) async {
    print("Some thing happen");
    try {
      var imageFile1 = await ImagePicker.pickImage(source: sourceFile);
      setState(() {
        //imageFile = imageFile1;
      });
    } catch (e) {
      print("Error " + e.toString());
    }
  }
}