import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/changePassword.dart';
import 'package:the_halal_people/profile/personalnfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';

class onlineSupport extends DrawerContent {

  @override
  _onlineSupportState createState() => _onlineSupportState();
}

class _onlineSupportState extends State<onlineSupport> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Send Message", style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/message.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Support 24/7",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Lorem ipsum dolor sit amet, consectetur non adipiscing elit. Etiam ac tempor leo.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 25),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
              child: Row(
                children: <Widget>[
                  Text("* Comments",
                    style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            customTextField(
              textFieldWidth: size.convertWidth(context, 301),
              textFieldHeight: size.convert(context, 170),
              radius: 5,
              mutiLine: true,
              bgColor: Colors.white,
            ),
            SizedBox(height: size.convert(context, 20),),
            swapButton(
              buttonColor: buttonColor,
              circleColor: Colors.transparent,
              textColor: Colors.white,
              buttonText: "SEND MESSAGE",
              buttonHieght: size.convert(context, 55),
              buttonWidth: size.convertWidth(context, 301),
              circleBorderColor: Colors.transparent,
              borderColor: Colors.transparent,
              fontfamily: "RobotoRegular",
            )

          ],),
      ),
    );
  }
}