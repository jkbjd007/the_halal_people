
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/changePassword.dart';
import 'package:the_halal_people/profile/personalnfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';

class personalSupport extends DrawerContent {

  @override
  _personalSupportState createState() => _personalSupportState();
}

class _personalSupportState extends State<personalSupport> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  List faq = [{"title":"LOREM IPSUM DOLOR","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":true},
    {"title":"SIT AMET CONCATEUR","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"PHASELLUS IACULIS","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"Vestibulum vitae","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},{"title":"LOREM IPSUM DOLOR","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"Mauris dapibus orci","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"SIT AMET CONCATEUR","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"Aliquam neque maximus","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"Sed sagittis Leo","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
    {"title":"Sed eleifend risus","detail":"Quisque sit amet felis vitae elit bibendum sollicitudin. Fusce sit amet enim viverra, pellentesque libero eu, gravida leo.\n\nPellentesque eu nisl vestibulum, eleifend mi vel, molestie ante. Praesent id mauris nisl.\n\nNunc aliquam turpis in rhoncus consequat. Curabitur et gravida enim, a ullamcorper massa.","isOpen":false},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Send Message", style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/messageBi.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Faq",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Lorem ipsum dolor sit amet, consectetur non adipiscing elit. Etiam ac tempor leo.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 25),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
              child: Row(
                children: <Widget>[
                  Text("* Comments",
                    style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
              child: DottedBorder(
                color:  Colors.grey,
                strokeWidth: 1,
                dashPattern: [8, 4],
                radius: Radius.circular(10),
                strokeCap: StrokeCap.round,
                borderType: BorderType.RRect,
                child: Container(
                  child: ListView.separated(
                    itemBuilder: (BuildContext context, int index){
                      return Container(
                        padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15),
                        vertical: size.convert(context, 15)),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                              Text(faq[index]["title"]??"",
                              style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),
                              faq[index]["isOpen"] ? InkWell(
                                onTap: (){
                                  setState(() {
                                    faq[index]["isOpen"] = false;
                                  });
                                },
                                  child: Icon(IcoFontIcons.minus)) : InkWell(
                                  onTap: (){
                                    setState(() {
                                      faq[index]["isOpen"] = true;
                                    });
                                  },
                                  child: Icon(IcoFontIcons.plus))
                            ],),
                            faq[index]["isOpen"] ? SizedBox(height: size.convert(context, 20),) : SizedBox(height: size.convert(context, 0),),
                            faq[index]["isOpen"] ? Container(child: RichText(text: TextSpan(
                              text: faq[index]["detail"]??"",style: styles.slideparaStyle(fontSize: size.convert(context, 13))
                            ),),) : SizedBox(height: size.convert(context, 0),),
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index){
                      return Divider();
                    },
                    itemCount: faq.length??0,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                  ),
                ),
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
          ],),
      ),
    );
  }
}