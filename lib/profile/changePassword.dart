import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/sign/otpVerification.dart';
class changePassword extends StatefulWidget {
  @override
  _changePasswordState createState() => _changePasswordState();
}

class _changePasswordState extends State<changePassword> {
  bool pasTextDis = false;
  bool rePasTextDis = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Change Password",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(height: size.convert(context, 40),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 132
            )),
            child: Image.asset("assets/icons/changePass.png",
              height: size.convert(context, 110),),
            width: size.convert(context, 110),
          ),
          SizedBox(height: size.convert(context, 15),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 67)),
            child:Text("Curabitur sit amet massa nunc. Fusce at tristique magna. Fusce eget dapibus dui.",
              style: styles.slideparaStyle(fontSize: 12),
              textAlign: TextAlign.center,
              textScaleFactor: 1,
            ),),
          SizedBox(height: size.convert(context, 30),),
          customTextField(
            obscureText: !pasTextDis,
            istralingIcon: true,
            tralingIcon: InkWell(
              onTap: (){
                setState(() {
                  pasTextDis = !pasTextDis;
                });
              },
              child: Container(width: size.convertWidth(context, 20),
              child: !pasTextDis ? Icon(IcoFontIcons.eyeBlocked,color: hintsColor,size: size.convert(context, 25),) : Icon(IcoFontIcons.eye,color: hintsColor,size: size.convert(context, 25),),
              ),
            ),
            isWidth: true,
            icons: Container(width: size.convertWidth(context, 20),),
            nameIcons: Image.asset("assets/icons/lockIcon.png"),
            textFieldWidth: size.convertWidth(context, 301),
            textFieldHeight: size.convert(context, 55),
            hints: "Password",
          ),
          SizedBox(height: 10,),
          customTextField(
            obscureText: !rePasTextDis,
            istralingIcon: true,
            tralingIcon: InkWell(
              onTap: (){
                setState(() {
                  rePasTextDis = !rePasTextDis;
                });
              },
              child: Container(width: size.convertWidth(context, 20),
                child: !rePasTextDis ? Icon(IcoFontIcons.eyeBlocked,color: hintsColor,size: size.convert(context, 25),) : Icon(IcoFontIcons.eye,color: hintsColor,size: size.convert(context, 25),),
              ),
            ),
            isWidth: true,
            icons: Container(width: size.convertWidth(context, 20),),
            nameIcons: Image.asset("assets/icons/lockIcon.png"),
            textFieldWidth: size.convertWidth(context, 301),
            textFieldHeight: size.convert(context, 55),
            hints: "Confirm Password",
          ),
          SizedBox(height: 10,),
          swapButton(
            onClick: (){
              Navigator.pushReplacement(context, PageTransition(child: otpVerification(),type: PageTransitionType.upToDown));
            },
            buttonWidth: size.convertWidth(context, 301),
            buttonHieght: size.convert(context, 58),
            buttonText: "SAVE",
            circleColor: Colors.transparent,
            buttonColor: Colors.black,
            textColor: Colors.white,
            circleBorderColor: Colors.transparent,
          ),

        ],),
      ),

    );
  }
}
