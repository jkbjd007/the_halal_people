import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/addimage.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
class personalInfo extends DrawerContent {
  @override
  _personalInfoState createState() => _personalInfoState();
}

class _personalInfoState extends State<personalInfo> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  String selectBusiness ;
  String selectFood ;
  ByteData _img = ByteData(0);
  var color = Colors.red;
  var strokeWidth = 5.0;
  final _sign = GlobalKey<SignatureState>();
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Personal Information",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      //margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
      child:
      SingleChildScrollView(child: Column(children: <Widget>[
        SizedBox(height: size.convert(context, 20),),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: (){
                _settingModalBottomSheet();
              },
              child: addImage(
                h: size.convert(context, 90),
                w: size.convert(context, 90),
              ),
            ),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Name, Last name",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Enter your new password and then click on the “Save” button below.",
                          style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                        )
                      ]
                  ),),
              )
            ],
          ),
        ),

        SizedBox(height: size.convert(context, 10),),
        customTextField(
          isWidth: true,
          icons: Container(width: size.convertWidth(context, 20),),
          nameIcons: Image.asset("assets/icons/person.png"),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Last, First Name",
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "+995 599771803",
          fontSize: size.convert(context, 15),
          fontFamily: "RobotoLight",
          fontColor: Colors.black.withOpacity(0.35),
          icons: Container(
            child: Row(children: <Widget>[
              SizedBox(width: 10,),
              Image.asset("assets/icons/flag_usa.png"),
              SizedBox(width: 10,),
              Icon(Icons.keyboard_arrow_down),
              SizedBox(width: 10,),
            ],),
          ),
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          isWidth: true,
          icons: Container(width: size.convertWidth(context, 20),),
          nameIcons: Image.asset("assets/icons/mail.png"),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Email address",
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          isWidth: true,
          icons: Container(width: size.convertWidth(context, 20),),
          nameIcons: Image.asset("assets/icons/personalId.png"),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Personal ID",
        ),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 80)),
          child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children:[
          TextSpan(text: "Please Upload You Personal ID Certificate Below",style: styles.RobotoCondensedRegular())
        ]
        ),),),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset("assets/icons/uploadIcon.png"),
              Text("FRONT",style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),),
              InkWell(
                  onTap: (){
                    print("press delete icons");
                  },
                  child: Icon(IcoFontIcons.uiDelete,color: Colors.red,size: size.convert(context,12),))
            ],),),
        SizedBox(height: size.convert(context, 10),),
        Container(
          width: size.convertWidth(context, 301),
          height: size.convert(context, 201),
          decoration: BoxDecoration(
            color: textFieldBgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: InkWell(
            onTap: (){
              _settingModalBottomSheet();
            },
            child: Center(child:  Container(
              width: size.convert(context, 63),
              height: size.convert(context, 63),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(500),
              ),
              child: Image.asset("assets/icons/cameraL.png"),
            ),
            ),
          ),
        ),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset("assets/icons/uploadIcon.png"),
              Text("BACK",style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),),
              InkWell(
                  onTap: (){
                    print("press delete icons");
                  },
                  child: Icon(IcoFontIcons.uiDelete,color: Colors.red,size: size.convert(context,12),))
            ],),),
        SizedBox(height: size.convert(context, 10),),
        Container(
          width: size.convertWidth(context, 301),
          height: size.convert(context, 201),
          decoration: BoxDecoration(
            color: textFieldBgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: InkWell(
            onTap: (){
              _settingModalBottomSheet();
            },
            child: Center(child:  Container(
              width: size.convert(context, 63),
              height: size.convert(context, 63),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(500),
              ),
              child: Image.asset("assets/icons/cameraL.png"),
            ),
            ),
          ),
        ),
        SizedBox(height: 10,),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convert(context, 50)),
          child: Row(
            children: <Widget>[
              Text("Signature",style: styles.slideparaStyle(fontSize: size.convert(context, 12),),),
            ],
          ),
        ),
        SizedBox(height: size.convert(context, 3),),
        Container(
          width: size.convertWidth(context, 301),
          height: size.convert(context, 201),
          decoration: BoxDecoration(
            color: textFieldBgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Signature(
            color: Colors.black,
            key: _sign,
            onSign: () {
              final sign = _sign.currentState;
              debugPrint('${sign.points.length} points in the signature');
            },
            backgroundPainter: _WatermarkPaint("2.0", "2.0"),
            strokeWidth: strokeWidth,
          ),
        ),
        SizedBox(height: size.convert(context, 20),),
        swapButton(
          buttonHieght: size.convert(context, 58),
          buttonWidth: size.convertWidth(context, 301),
          buttonColor: Colors.black,
          buttonText: "SAVE",
          textColor: Colors.white,
          circleColor: Colors.transparent,
          onClick: (){
          },
        ),
        SizedBox(height: size.convert(context, 20),),
      ],),),);
  }
  _settingModalBottomSheet() {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: size.convert(context, 90),
            color: Colors.white,
            padding: EdgeInsets.symmetric(
                horizontal: size.convert(context, 22),
                vertical: size.convert(context, 16)),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    print("Take a Photo");
                    _onImageButtonPressed(ImageSource.camera);
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: size.convert(context, 10)),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "take A Photo",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    print("Upload from gallery");
                    Navigator.pop(context);
                    _onImageButtonPressed(ImageSource.gallery);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: size.convert(context, 10),
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "Upload From Gallery",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
  _onImageButtonPressed(ImageSource sourceFile) async {
    print("Some thing happen");
    try {
      var imageFile1 = await ImagePicker.pickImage(source: sourceFile);
      setState(() {
        //imageFile = imageFile1;
      });
    } catch (e) {
      print("Error " + e.toString());
    }
  }
}
class _WatermarkPaint extends CustomPainter {
  final String price;
  final String watermark;

  _WatermarkPaint(this.price, this.watermark);

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    canvas.drawCircle(Offset(size.width / 2, size.height / 2), 10.8, Paint()..color = Colors.transparent);
  }

  @override
  bool shouldRepaint(_WatermarkPaint oldDelegate) {
    return oldDelegate != this;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is _WatermarkPaint && runtimeType == other.runtimeType && price == other.price && watermark == other.watermark;

  @override
  int get hashCode => price.hashCode ^ watermark.hashCode;
}
