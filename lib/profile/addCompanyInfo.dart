import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/addimage.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
class addCompanyInfo extends DrawerContent {
  @override
  _addCompanyInfoState createState() => _addCompanyInfoState();
}

class _addCompanyInfoState extends State<addCompanyInfo> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  String selectBusiness ;
  String selectFood ;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Company / Store Information",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      //margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
      child:
      SingleChildScrollView(child: Column(children: <Widget>[
        SizedBox(height: size.convert(context, 20),),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: (){
                _settingModalBottomSheet();
              },
              child: addImage(
                h: size.convert(context, 90),
                w: size.convert(context, 90),
              ),
            ),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Restaurant Name",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Please choose your business type and add your company information.",
                          style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                        )
                      ]
                  ),),
              )
            ],
          ),
        ),
        SizedBox(height: size.convert(context, 10),),
        DottedBorder(
          color:  Colors.grey,
          strokeWidth: 1,
          dashPattern: [8, 4],
          radius: Radius.circular(50),
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: Container(
            width: size.convertWidth(context, 301),
            height: size.convert(context, 55),
            decoration: BoxDecoration(
                color: textFieldBgColor,
                borderRadius: BorderRadius.circular(50)
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 27),
              ),
              child: Center(
                child: DropdownButton(
                  underline: Container(),
                  style: styles.hintsStyle(),
//              focusColor: Colors.blue,
                  isExpanded: true,
                  icon: Icon(Icons.keyboard_arrow_down,color: hintsColor,),
                  hint: Text("Type of Business"),
                  // Not necessary for Option 1
                  value: selectBusiness,
                  onChanged: (newValue) {
                    setState(() {
                      selectBusiness = newValue;
                    });
                  },

                  items: businessType.map((title) {
                    return DropdownMenuItem(
                      child: new Text(title),
                      value: title,
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          icons: Container(width: size.convertWidth(context, 25),),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Number of Company",
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          icons: Container(width: size.convertWidth(context, 25),),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Company Name",
        ),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convert(context, 50)),
          child: Row(
            children: <Widget>[
              Text("Type of Restaurant (Cafe)",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12),),),
            ],
          ),
        ),
        SizedBox(height: size.convert(context, 3),),
        DottedBorder(
          color:  Colors.grey,
          strokeWidth: 1,
          dashPattern: [8, 4],
          radius: Radius.circular(10),
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: Container(
            width: size.convertWidth(context, 301),
            height: size.convert(context, 55),
            decoration: BoxDecoration(
                color: textFieldBgColor,
                borderRadius: BorderRadius.circular(10)
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 27),
              ),
              child: Center(
                child: DropdownButton(
                  underline: Container(),
                  style: styles.hintsStyle(),
//              focusColor: Colors.blue,
                  isExpanded: true,
                  icon: Icon(Icons.keyboard_arrow_down,color: hintsColor,),
                  hint: Text("Fast Food"),
                  // Not necessary for Option 1
                  value: selectFood,
                  onChanged: (newValue) {
                    setState(() {
                      selectFood = newValue;
                    });
                  },

                  items: foodType.map((title) {
                    return DropdownMenuItem(
                      child: new Text(title),
                      value: title,
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: size.convert(context, 15),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
        Image.asset("assets/icons/uploadIcon.png"),
          Text("Store Picture (front side)",style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),),
          InkWell(
            onTap: (){
              print("press delete icons");
            },
              child: Icon(IcoFontIcons.uiDelete,color: Colors.red,size: size.convert(context,12),))
        ],),),
        SizedBox(height: size.convert(context, 10),),
        Container(
          width: size.convertWidth(context, 301),
          height: size.convert(context, 201),
          decoration: BoxDecoration(
          color: textFieldBgColor,
          borderRadius: BorderRadius.circular(10),
        ),
          child: InkWell(
            onTap: (){
              _settingModalBottomSheet();
            },
            child: Center(child:  Container(
                width: size.convert(context, 63),
                height: size.convert(context, 63),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(500),
                ),
                child: Image.asset("assets/icons/cameraL.png"),
              ),
            ),
          ),
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          icons: Container(width: size.convertWidth(context, 25),),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Identification Number",
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          icons: Container(width: size.convertWidth(context, 25),),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Country",
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          icons: Container(width: size.convertWidth(context, 25),),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "City",
        ),
        SizedBox(height: size.convert(context, 10),),
        customTextField(
          icons: Container(width: size.convertWidth(context, 25),),
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Phisical address (store loacation)",
        ),
        SizedBox(height: size.convert(context, 10),),
        Container(child: Image.asset("assets/icons/googlemap.png"),),
        SizedBox(height: size.convert(context, 10),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convert(context, 50)),
          child: Row(
            children: <Widget>[
              Text("Editional information",style: styles.slideparaStyle(fontSize: size.convert(context, 12),),),
            ],
          ),
        ),
        customTextField(
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 100),
          hints: "Editional information for the delivery service for better finding you store or your location.",
          radius: 10,
          mutiLine: true,
        ),
        SizedBox(height: size.convert(context, 20),),
        swapButton(
          buttonHieght: size.convert(context, 58),
          buttonWidth: size.convertWidth(context, 301),
          buttonColor: Colors.black,
          buttonText: "SAVE",
          textColor: Colors.white,
           circleColor: Colors.transparent,
          onClick: (){
          },
        ),
        SizedBox(height: size.convert(context, 20),),
      ],),),);
  }
  _settingModalBottomSheet() {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: size.convert(context, 90),
            color: Colors.white,
            padding: EdgeInsets.symmetric(
                horizontal: size.convert(context, 22),
                vertical: size.convert(context, 16)),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    print("Take a Photo");
                    _onImageButtonPressed(ImageSource.camera);
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: size.convert(context, 10)),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "take A Photo",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    print("Upload from gallery");
                    Navigator.pop(context);
                    _onImageButtonPressed(ImageSource.gallery);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: size.convert(context, 10),
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "Upload From Gallery",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
  _onImageButtonPressed(ImageSource sourceFile) async {
    print("Some thing happen");
    try {
      var imageFile1 = await ImagePicker.pickImage(source: sourceFile);
      setState(() {
        //imageFile = imageFile1;
      });
    } catch (e) {
      print("Error " + e.toString());
    }
  }
}
