import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
class orderHistory extends StatefulWidget {
  @override
  _orderHistoryState createState() => _orderHistoryState();
}

class _orderHistoryState extends State<orderHistory> {
  bool isEmpty = false;
  List withDraw = [{"icon":"assets/icons/mastercard.png","date":"Bruno Bernshtein","orderId":"886402","price":"\$345.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"Roberto Gandolini","orderId":"886402","price":"\$3145.90"},
    {"icon":"assets/icons/bank.png","date":"Laura Cliford","orderId":"886402","price":"\$45.90"},
    {"icon":"assets/icons/mastercard.png","date":"David Brown","orderId":"886402","price":"\$345.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"Roberto Gandolini","orderId":"886402","price":"\$3453.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"Nika Baratashvili","orderId":"886402","price":"\$3435.90"},
    {"icon":"assets/icons/mastercard.png","date":"Sara Bernar","orderId":"886402","price":"\$345.90"},
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(child: Column(children: <Widget>[
        isEmpty ? emptyBody() : body(),
      ],),),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 34)),
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                   Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                      Text("BALANCE",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                      SizedBox(height: size.convert(context, 3),),
                      Text("\$234.30",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 25),color: Color(0xff1308fe)),)
                    ],),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                    Text("TOTAL EARNED",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                    SizedBox(height: size.convert(context, 3),),
                    Text("\$33,346.50",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 25),color: Colors.black),)
                  ],)
                ],
              ),),
            SizedBox(height: size.convert(context, 15),),
//            Row(
//              children: <Widget>[
//                Container(
//                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 43)),
//                  child: Text("Add Amount"),
//                ),
//              ],
//            ),
//            SizedBox(height: size.convert(context, 3),),
            Container(
              width: size.convertWidth(context, 335),
              height: size.convert(context, 55),
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 34)),
              child: searchTextField(
                textFieldWidth :size.convertWidth(context, 335),
                  textFieldHeight: size.convert(context, 55),
                istralingIcon: true,
                tralingIcon: Image.asset("assets/icons/search.png"),
                isPadding: true,
                textInputType: TextInputType.number,
                radius: size.convert(context, 5),
                bgColor: Colors.white,
              ),
            ),

            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("All Sales",
                    style: styles.slideHeadingStyle(fontSize: size.convert(context, 16)),),

                  Row(
                    children: <Widget>[
                      Text("Date:",
                        style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                      Image.asset("assets/icons/downArrow.png")
                    ],
                  ),
                ],),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: DottedBorder(
                color:  Colors.grey,
                strokeWidth: 1,
                dashPattern: [8, 4],
                radius: Radius.circular(10),
                strokeCap: StrokeCap.round,
                borderType: BorderType.RRect,
                child: ListView.separated(
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index){
                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20),vertical: size.convert(context, 20)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                circularImage(
                                  h: size.convert(context, 50),
                                  w: size.convert(context, 50),
                                ),
                                SizedBox(width: size.convertWidth(context, 20),),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(withDraw[index]["date"]??"",
                                      style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),),),
                                   RichText(text: TextSpan(children:[
                                     TextSpan(
                                         text:"Invoice No: ",
                                           style: styles.pageTitleStyle(fontSize: 12, color: Colors.black.withOpacity(0.6)),
                                     ),
                                     TextSpan(
                                       text:"${withDraw[index]["orderId"]??""}",
                                       style: TextStyle(
                                           fontSize: size.convert(context, 12),
                                           color: Colors.black,
                                           fontFamily: "RobotoCondensed-Regular",
                                            decoration: TextDecoration.underline
                                       ),
                                     )
                                   ]),)
                                  ],)
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,

                              children: <Widget>[
                                Container(child: Text(withDraw[index]["price"]??"",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),color: Color(0xff1308fe)),),),
                                Container(child: Text("23.04.2020",style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),),),
                                Container(child: Text("0",style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),),),

                              ],
                            )
                          ],),);
                    },
                    separatorBuilder: (BuildContext context, int index){
                      return Divider(

                      );
                    },
                    itemCount: withDraw.length ?? 0),
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            swapButton(
              buttonWidth: size.convertWidth(context, 335),
              buttonHieght
                  : size.convert(context, 55),
              buttonText: "LOAD MORE",
              buttonColor: Colors.black,
              textColor: Colors.white,
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              fontfamily: "RobotoCondensed-Regular",
            ),
            SizedBox(height: size.convert(context, 20),),
          ],),
      ),

    );
  }
  emptyBody(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 34)),
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("BALANCE",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                      SizedBox(height: size.convert(context, 3),),
                      Text("\$0.00",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 25),color: Color(0xff1308fe)),)
                    ],),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text("TOTAL EARNED",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                      SizedBox(height: size.convert(context, 3),),
                      Text("\$0.00",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 25),color: Colors.black),)
                    ],)
                ],
              ),),
            SizedBox(height: size.convert(context, 15),),
//            Row(
//              children: <Widget>[
//                Container(
//                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 43)),
//                  child: Text("Add Amount"),
//                ),
//              ],
//            ),
//            SizedBox(height: size.convert(context, 3),),
            Container(
              width: size.convertWidth(context, 335),
              height: size.convert(context, 55),
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 34)),
              child: searchTextField(
                textFieldWidth :size.convertWidth(context, 335),
                textFieldHeight: size.convert(context, 55),
                istralingIcon: true,
                tralingIcon: Image.asset("assets/icons/search.png"),
                isPadding: true,
                textInputType: TextInputType.number,
                radius: size.convert(context, 5),
                bgColor: Colors.white,
              ),
            ),
          ],),
      ),

    );
  }
}
