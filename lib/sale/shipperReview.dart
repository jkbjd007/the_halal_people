import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/string.dart';
class shipperReview extends StatefulWidget {
  @override
  _shipperReviewState createState() => _shipperReviewState();
}

class _shipperReviewState extends State<shipperReview> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("shipper Review",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(child: Column(
        children: <Widget>[
          SizedBox(height: size.convert(context, 20),),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: (){
                  _settingModalBottomSheet();
                },
                child: circularImage(
                  h: size.convert(context, 72),
                  w: size.convert(context, 72),
                ),
              ),
            ],
          ),
          SizedBox(height: size.convert(context, 8),),
          Text("David Grinberg",
          style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 20)),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 80)),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text:"Please rate, your feedback will help improve product experience",
                  style: styles.slideparaStyle(fontSize: size.convert(context, 12),
              ),
              )),
          ),
          SizedBox(height: size.convert(context, 45),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
            child: Row(
              children: <Widget>[
                Text("* Comments",
                style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
              ],
            ),
          ),
          SizedBox(height: size.convert(context, 10),),
          customTextField(
            textFieldWidth: size.convertWidth(context, 301),
            textFieldHeight: size.convert(context, 170),
            radius: 5,
            mutiLine: true,
            bgColor: Colors.white,
          ),
          SizedBox(height: size.convert(context, 20),),
          swapButton(
            buttonColor: buttonColor,
            circleColor: Colors.transparent,
            textColor: Colors.white,
            buttonText: "ADD REVIEW",
            buttonHieght: size.convert(context, 55),
            buttonWidth: size.convertWidth(context, 301),
            circleBorderColor: Colors.transparent,
            borderColor: Colors.transparent,
            fontfamily: "RobotoRegular",
          )

        ],
      ),),
    );
  }
  _settingModalBottomSheet() {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: size.convert(context, 90),
            color: Colors.white,
            padding: EdgeInsets.symmetric(
                horizontal: size.convert(context, 22),
                vertical: size.convert(context, 16)),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    print("Take a Photo");
                    _onImageButtonPressed(ImageSource.camera);
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: size.convert(context, 10)),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "take A Photo",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    print("Upload from gallery");
                    Navigator.pop(context);
                    _onImageButtonPressed(ImageSource.gallery);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: size.convert(context, 10),
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "Upload From Gallery",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
  _onImageButtonPressed(ImageSource sourceFile) async {
    print("Some thing happen");
    try {
      var imageFile1 = await ImagePicker.pickImage(source: sourceFile);
      setState(() {
        //imageFile = imageFile1;
      });
    } catch (e) {
      print("Error " + e.toString());
    }
  }
}
