import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/sale/invoiceDeline.dart';
import 'package:the_halal_people/sale/invoicePaid.dart';

import 'invoiceDelivered.dart';
class invoiceHistory extends StatefulWidget {
  @override
  _invoiceHistoryState createState() => _invoiceHistoryState();
}

class _invoiceHistoryState extends State<invoiceHistory> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/profileInfo.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Invoice History",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Lorem ipsum dolor sit amet, consectetur non adipiscing elit. Etiam ac tempor leo.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: invoicePaid(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/paid.png",
                  height: size.convert(context, 30),
                    width: size.convert(context, 30),
                  ),
                  buttonText: "Paid Invoice",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
            SizedBox(height: size.convert(context, 10),),
            Container(

                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: invoiceDeline(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/declined.png",
                    height: size.convert(context, 30),
                    width: size.convert(context, 30),
                  ),
                  buttonText: "Decline Invoice",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
            SizedBox(height: size.convert(context, 10),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: invoiceDelivered(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/delivered.png",
                    height: size.convert(context, 30),
                    width: size.convert(context, 30),
                  ),
                  buttonText: "Delivered Invoice",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
          ],),
      ),
    );
  }
}
