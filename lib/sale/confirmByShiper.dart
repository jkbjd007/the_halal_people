import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/sale/shipperReview.dart';
class confirmByShiper extends StatefulWidget {
  @override
  _confirmByShiperState createState() => _confirmByShiperState();
}

class _confirmByShiperState extends State<confirmByShiper> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Receipt / Surrender Act",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
      child: SingleChildScrollView(child:
      Column(
        children: <Widget>[
          SizedBox(height: size.convert(context, 25),),
          Row(children: <Widget>[
            RichText(text: TextSpan(
              text: "Receipt / Surrender Act  No DE354",
              style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 21))
            ),)
          ],),
          SizedBox(height: size.convert(context, 22),),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            RichText(text: TextSpan(
                text: "[Name of city]",
                style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
            ),),
              RichText(text: TextSpan(
                  text: "[Date]",
                  style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
              ),),
          ],),
          SizedBox(height: size.convert(context, 22),),
          Column(
            children: <Widget>[
              RichText(text: TextSpan(children: [
                   TextSpan(
                    text: "Lorem ipsum dolor sit amet concateur",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " [name of driver] ",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: "non troppo di sarono, ambocadi la trie",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " [name of seller] ",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " (la due as produtto sellero) imbsaduone.",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),

              ]),),
              SizedBox(height: size.convert(context, 30),),
              RichText(text: TextSpan(children: [

                TextSpan(
                    text: "[year] ",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " year",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " [date] ",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " Produtione la prada sesaturo la rico del prazo nue. Ut a lectus ac est aliquam posuere eget nec metus lido pradio selucosa inglucione",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: "  [year] ",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: " rivo dela",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: "  [Date] ",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                ),
                TextSpan(
                    text: "  ordinare la produtione dis sel rufo non frigo.",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                ),

              ]),),
              SizedBox(height: size.convert(context, 30),),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(text: TextSpan(children: [

                      TextSpan(
                          text: "Products total price is:",
                          style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                      ),
                      TextSpan(
                          text: "  [price of items] ",
                          style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13))
                      ),
                      TextSpan(
                          text: "USD",
                          style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                      ),

                    ]),),
                  ),
                ],
              ),
              SizedBox(height: size.convert(context, 30),),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(text: TextSpan(children: [

                      TextSpan(
                          text: "Pellentesque rutrum nibh vel neque facilisis, eu posuere sem molestie. Vivamus rutrum at tortor at porttitor. Ut a lectus ac est aliquam posuere eget nec metus. ",
                          style: styles.pageTitleStyle(fontSize: size.convert(context, 13))
                      ),
                    ]),),
                  ),
                ],
              ),
              SizedBox(height: size.convert(context, 30),),
            ],),
          SizedBox(height: size.convert(context, 22),),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Seller:",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 13)),
                    ),
                    Text("[name of seller]",
                      style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13)),
                    ),
                    Image.asset("assets/icons/sellerSign.png"),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text("Shipper:",
                      style: styles.pageTitleStyle(fontSize: size.convert(context, 13)),
                    ),
                    Text("[name of shipper]",
                      style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13)),
                    ),
                    InkWell(
                      onTap: (){
                          Navigator.push(context, PageTransition(child: shipperReview(),
                          type: PageTransitionType.rightToLeft));
                      },
                      child: Text("Shippers’ Review",
                        style: TextStyle(
                          fontSize: size.convert(context, 13),
                          color: Color(0xffff0000),
                          fontFamily: "RobotoCondensed-Light",
                          decoration: TextDecoration.underline
                        ),
                      ),
                    ),
                    Image.asset("assets/icons/shipperSign.png"),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: size.convert(context, 22),),
          Row(children: <Widget>[
            swapButton(
              buttonWidth: size.convertWidth(context, 165),
              buttonHieght: size.convert(context, 55),
              buttonColor: Color(0xffc8171d),
              buttonText: "DECLINE",
              radius: 5,
              borderColor: Colors.transparent,
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              fontfamily: "RobotoMedium",
              textColor: Colors.white,
            ),
            SizedBox(width: size.convertWidth(context, 8,)),
            swapButton(
              buttonWidth: size.convertWidth(context, 165),
              buttonHieght: size.convert(context, 55),
              buttonColor: Color(0xff3b5998),
              buttonText: "CONFIRM",
              radius: 5,
              borderColor: Colors.transparent,
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              fontfamily: "RobotoMedium",
              textColor: Colors.white,
            ),
          ],),
          SizedBox(height: size.convert(context, 22),),
        ],
      ),),);
  }
}
