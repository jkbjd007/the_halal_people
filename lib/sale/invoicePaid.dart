

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/sale/confirmByShiper.dart';
class invoicePaid extends StatefulWidget {
  @override
  _invoicePaidState createState() => _invoicePaidState();
}

class _invoicePaidState extends State<invoicePaid> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  List items = [{"name":"Cacke “Marbella”","icons":"assets/Products/CackeMarbella.png","detail":"Cras blandit consequat sapien ut cursus.","price":"2.13","count":1},
    {"name":"Chocomocco","icons":"assets/Products/Chocomocco.png","detail":"Cras blandit consequat sapien ut cursus.","price":"11.13","count":7},
    {"name":"Cupcake “Choco-Cherry”","icons":"assets/Products/CupcakeChocoCherry.png","detail":"Cras blandit consequat sapien ut cursus.","price":"2.13","count":4},
    {"name":"“Brawn” Cupcake","icons":"assets/Products/BrawnCupcake.png","detail":"Cras blandit consequat sapien ut cursus.","price":"15.13","count":1},
    {"name":"Cupcake “Choco-Nuts”","icons":"assets/Products/CupcakeChocoNuts.png","detail":"Cras blandit consequat sapien ut cursus.","price":"12.13","count":3},
    {"name":"Cupcake “Choco-Nuts”","icons":"assets/Products/CupcakeChocoNuts.png","detail":"Cras blandit consequat sapien ut cursus.","price":"23.13","count":9}
  ];
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Invoice Paid",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
      child: SingleChildScrollView(child:
      Column(
        children: <Widget>[
          SizedBox(height: size.convert(context, 25),),
          invoiceInfo(),
          item(),
          SizedBox(height: size.convert(context, 20),),
          Stemp(),
          SizedBox(height: size.convert(context, 20),),
          price(),
          SizedBox(height: size.convert(context, 20),),
        ],
      ),),);
  }
  invoiceInfo(){
    return Container(
      child: Column(children: <Widget>[
        Row(
          children: <Widget>[
            RichText(text: TextSpan(
                children: [
                  TextSpan(
                      text: "Invoice: ",
                      style: styles.RobotoCondensedRegular(fontSize: 25)
                  ),
                  TextSpan(
                      text: "30WT43GD54",
                      style: styles.pageTitleStyle(fontSize: 25)
                  ),

                ]
            ),),
          ],
        ),
        SizedBox(height: size.convert(context, 5),),
        Row(
          children: <Widget>[
            RichText(text: TextSpan(
                children: [
                  TextSpan(
                      text: "Date: 27 April 2020",
                      style: styles.pageTitleStyle(fontSize: 13)
                  ),

                ]
            ),),
          ],
        ),

        SizedBox(height: size.convert(context, 15),),
        Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          color: Colors.black,
                          width: 1
                      )
                  )
              ),
              child: RichText(text: TextSpan(
                  children: [
                    TextSpan(
                        text: "Invoice From:",
                        style: styles.hintsStyle(fontSize: size.convert(context, 13),
                            color: Colors.black)
                    ),

                  ]
              ),),
            ),
          ],
        ),
        SizedBox(height: size.convert(context, 3),),
        Row(
          children: <Widget>[
            Expanded(
              child: RichText(text: TextSpan(
                  children: [
                    TextSpan(
                        text: "“Monicas’ Trattoria”",
                        style: styles.hintsStyle(fontSize: size.convert(context, 13),
                            color: Colors.black)
                    ),
                    TextSpan(
                        text: "- 50, Salem str.  4352.,Boston., USA.",
                        style: styles.slideparaStyle(fontSize: size.convert(context, 13),
                            color: Colors.black)
                    ),

                  ]
              ),),
            ),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),

        Divider(),
        Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          color: Colors.black,
                          width: 1
                      )
                  )
              ),
              child: RichText(text: TextSpan(
                  children: [
                    TextSpan(
                        text: "Deliver To:",
                        style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 14),
                            color: Colors.black)
                    ),

                  ]
              ),),
            ),
          ],
        ),
        SizedBox(height: size.convert(context, 3),),
        Row(
          children: <Widget>[
            Expanded(
              child: RichText(text: TextSpan(
                  children: [
                    TextSpan(
                        text: "“Monicas’ Trattoria”",
                        style: styles.hintsStyle(fontSize: size.convert(context, 13),
                            color: Colors.black)
                    ),
                    TextSpan(
                        text: "- 50, Salem str.  4352.,Boston., USA.",
                        style: styles.slideparaStyle(fontSize: size.convert(context, 13),
                            color: Colors.black)
                    ),

                  ]
              ),),
            ),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),
        Divider(),
      ],),
    );
  }

  item(){
    return Container(
      child: ListView.separated(
        itemBuilder: (BuildContext context, int index){
          return DottedBorder(
            color:  Colors.grey,
            strokeWidth: 1,
            dashPattern: [20, 6],
            radius: Radius.circular(10),
            strokeCap: StrokeCap.round,
            borderType: BorderType.RRect,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
                  vertical: size.convert(context, 10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      circularImage(
                        w: size.convert(context, 85),
                        h: size.convert(context, 72),
                        isProduct: true,
                        assetImage: true,
                        imageUrl: items[index]["icons"],
                        radius: 5,
                      ),
                      SizedBox(width: size.convertWidth(context, 10),),
                      Container(
                        //color: Colors.red,
                        width: size.convertWidth(context, 130),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(items[index]["name"]??"some ths",
                              style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13)),),
                            Text("Cras blandit consequat sapien ut cursus. ",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 11)),)
                          ],
                        ),
                      ),
                    ],
                  ),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("\$${items[index]["price"]??"2.23"}",
                        style: styles.RobotoCondensedBold(fontSize: size.convert(context, 13),
                            color: Colors.red),),
                      SizedBox(height: size.convert(context, 10),),
                      Container(child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              setState(() {
                                if(items[index]["count"]>0)
                                items[index]["count"] = items[index]["count"] - 1 ;
                              });
                            },
                            child: Icon(IcoFontIcons.minus,
                              size: 15,),
                          ),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 5)),
                              child: Text("${items[index]["count"]??"1"}")),
                          InkWell(
                            onTap: (){
                              setState(() {
                                items[index]["count"] = items[index]["count"] + 1 ;
                              });
                            },
                            child: Icon(IcoFontIcons.plus,
                              size: 15,),
                          )
                        ],
                      ),)
                    ],
                  )
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index){
          return Container(height: size.convert(context, 10),);
        },
        itemCount: items.length??0,
      physics: ScrollPhysics(),
      shrinkWrap: true,
    ),
    );

  }
  Stemp(){
    return Container(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerRight,
              child: Image.asset("assets/icons/paid.png")),
          Column(children: <Widget>[
            Row(
              children: <Widget>[
                RichText(text: TextSpan(
                    children: [
                      TextSpan(
                          text: "Invoice To:",
                          style: styles.hintsStyle(fontSize: size.convert(context, 13),
                              color: Colors.black)
                      ),

                    ]
                ),),
              ],
            ),
            SizedBox(height: size.convert(context, 3),),
            Row(
              children: <Widget>[
                Expanded(
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: "71, Charter str. App. 5r., Boston,MA, USA",
                            style: styles.pageTitleStyle(fontSize: size.convert(context, 14),
                                color: Colors.black)
                        ),
                      ]
                  ),),
                ),
              ],
            ),
            SizedBox(height: size.convert(context, 3),),
            InkWell(
              onTap: (){
                Navigator.push(context, PageTransition(child: confirmByShiper(),type: PageTransitionType.upToDown));
              },
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Surrender Act:",
                              style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 14),
                                  color: Colors.black)
                          ),
                        ]
                    ),),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: size.convert(context, 10)),
                child: Divider()),
            Row(
              children: <Widget>[
                Expanded(
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: "Delivery Date & Time:",
                            style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 14), color: Colors.black)
                        ),
                      ]
                  ),),
                ),
              ],
            ),
            SizedBox(height: size.convert(context, 3),),
            Row(
              children: <Widget>[
                SvgPicture.asset("assets/bottomMenu/scheduleDis.svg"),
                SizedBox(width: size.convert(context, 5),),
                 RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: "17 September 2020",
                            style: styles.pageTitleStyle(fontSize: size.convert(context, 14), color: Colors.black)
                        ),
                      ]
                  ),),
                SizedBox(width: size.convert(context,15),),
                Container(
                  width: 11,
                  height: 11,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    shape: BoxShape.circle
                  ),
                ),
                SizedBox(width: size.convert(context, 5),),
                RichText(text: TextSpan(
                    children: [
                      TextSpan(
                          text: "at - 10:00",
                          style: styles.pageTitleStyle(fontSize: size.convert(context, 14), color: Colors.black)
                      ),
                    ]
                ),),


              ],
            ),



          ],),
        ],
      ),
    );
  }
  price(){
    return Container(
      child: Column(children: <Widget>[
        Divider(),
        SizedBox(height: size.convert(context, 20),),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text:"Total: ",
                  style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16))
                )
              ]
            ),
          ),
            RichText(
              text: TextSpan(
                  children: [
                    TextSpan(
                        text:"\$140.95",
                        style: styles.RobotoCondensedBold(fontSize: size.convert(context, 16),
                        color: Color(0xffc8171d))
                    )
                  ]
              ),
            ),
        ],)
      ],),
    );
  }
}
