import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/messanger/notification.dart';
import 'package:the_halal_people/sale/mainSaleSlider.dart';
import 'package:the_halal_people/profile/profileInfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/schedule/schedule.dart';
import 'package:the_halal_people/product/mainProductSlider.dart';
class mainMenu extends DrawerContent {
  int pageNumber;
  mainMenu({this.pageNumber = 3});
  @override
  _mainMenuState createState() => _mainMenuState();
}

class _mainMenuState extends State<mainMenu> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  CarouselController _controller = CarouselController();
  //int selectedBody ;
  @override
  Widget build(BuildContext context) {
    print("fffffffffff${widget.pageNumber}");
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
          trailingIcon: InkWell(
              onTap: widget.onMenuPressed,
              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text(titleSelection(widget.pageNumber),style: styles.pageTitleStyle(),),
          leadingIcon: Image.asset("assets/icons/rightArrowIcon.png"),
        ),
      ),
      body: bodySelection(widget.pageNumber),
      bottomNavigationBar: customAppBar(),
    );
  }
  Widget bodySelection(int check){
    if(check == 1){
      return mainProductSlider();
    }
    else if(check == 2){
      return schedule();
    }
    else if(check == 3){
      return profileInfo();
    }
    else if(check == 4){
      return notification();
    }
    else if(check == 5){
      return mainSaleSlider();
    }
  }
  String titleSelection(int check){
    if(check == 1){
      return "Products";
    }
    else if(check == 2){
      return "Schedule";
    }
    else if(check == 3){
      return "Profile";
    }
    else if(check == 4){
      return "Notification";
    }
    else if(check == 5){
      return "Sale";
    }
  }

  customAppBar(){
    TextStyle unselectTextstyle = TextStyle(
      fontFamily: "RobotoRegular",
      fontSize: 8,
      color: Colors.black,
    );
    TextStyle selectTextstyle = TextStyle(
      fontFamily: "RobotoRegular",
      fontSize: 8,
      color: Colors.black,

    );
    return DottedBorder(
        color:  Colors.grey,
        strokeWidth: 0.5,
        dashPattern: [9, 9],
        radius: Radius.circular(10),
    strokeCap: StrokeCap.round,
    borderType: BorderType.RRect,
      child: Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: textFieldBgColor,
        borderRadius: BorderRadius.circular(50)
      ),
      height: size.convert(context, 60),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: InkWell(
              onTap: () {
                print("1");
                setState(() {
                  widget.pageNumber =1;
                });
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.pageNumber == 1?
                    SvgPicture.asset("assets/bottomMenu/productActive.svg")
                        :SvgPicture.asset("assets/bottomMenu/productDis.svg"),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Products",
                      style: widget.pageNumber == 1 ? selectTextstyle : unselectTextstyle,
                    ),
                    widget.pageNumber == 1 ? SizedBox(height: 3):SizedBox(height: 0),
                    widget.pageNumber == 1 ? Container(width: 33,height: 1,color: Colors.black,):Container(),
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: InkWell(
              onTap: () {
                print("2");
                setState(() {
                  widget.pageNumber =2;
                });
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.pageNumber == 2?
                    SvgPicture.asset("assets/bottomMenu/scheduleActive.svg")
                        :SvgPicture.asset("assets/bottomMenu/scheduleDis.svg"),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Schedule",
                      style: widget.pageNumber == 2 ? selectTextstyle : unselectTextstyle,
                    ),
                    widget.pageNumber == 2 ? SizedBox(height: 3):SizedBox(height: 0),
                    widget.pageNumber == 2 ? Container(width: 35,height: 1,color: Colors.black,):Container(),
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: InkWell(
              onTap: () {
                print("3");
                setState(() {
                  widget.pageNumber =3;
                });
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.pageNumber == 3?
                    SvgPicture.asset("assets/bottomMenu/accountActive.svg")
                        :SvgPicture.asset("assets/bottomMenu/accountdis.svg"),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Account",
                      style: widget.pageNumber == 1 ? selectTextstyle : unselectTextstyle,
                    ),
                    widget.pageNumber == 3 ? SizedBox(height: 3):SizedBox(height: 0),
                    widget.pageNumber == 3 ? Container(width: 30,height: 1,color: Colors.black,):Container(),
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: InkWell(
              onTap: () {
                print("4");
                setState(() {
                  widget.pageNumber =4;
                });
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.pageNumber == 4?
                    SvgPicture.asset("assets/bottomMenu/notificationActive.svg")
                        :SvgPicture.asset("assets/bottomMenu/notificationDis.svg"),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Notification",
                      style: widget.pageNumber == 4 ? selectTextstyle : unselectTextstyle,
                    ),
                    widget.pageNumber == 4 ? SizedBox(height: 3):SizedBox(height: 0),
                    widget.pageNumber == 4 ? Container(width: 40,height: 1,color: Colors.black,):Container(),
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: InkWell(
              onTap: () {
                print("5");
                setState(() {
                  widget.pageNumber =5;
                });
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    widget.pageNumber == 5?
                    SvgPicture.asset("assets/bottomMenu/saleActive.svg")
                        :SvgPicture.asset("assets/bottomMenu/saleDis.svg"),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Sale",
                      style: widget.pageNumber == 5 ? selectTextstyle : unselectTextstyle,
                    ),
                    widget.pageNumber == 5 ? SizedBox(height: 3):SizedBox(height: 0),
                    widget.pageNumber == 5 ? Container(width: 20,height: 1,color: Colors.black,):Container(),
                  ],
                ),
              ),
            ),
          ),
//          GestureDetector(
//            onTap: () {
//              setState(() {
//                widget.select = 4;
//                getIt<GlobalSingleton>().navigationKey.currentState.push(
//                    context,
//                    PageTransition(
//                        type: PageTransitionType.fade, child: Blog()));
//              });
//            },
//            child: Container(
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  Image.asset(
//                    "assets/icons/blog.png",
//                    color: widget.select == 4 ? appColor : unSelectedColor,
//                  ),
//                  SizedBox(
//                    height: 5,
//                  ),
//                  Text(
//                    "View Blogs",
//                    style: widget.select == 4 ? selectTextstyle : unselectTextstyle,
//                  )
//                ],
//              ),
//            ),
//          ),
        ],
      ),
    ));
  }

}