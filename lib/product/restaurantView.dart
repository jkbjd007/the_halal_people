import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
class restaurantView extends StatefulWidget {
  @override
  _restaurantViewState createState() => _restaurantViewState();
}

class _restaurantViewState extends State<restaurantView> {
  List foodCategories = [
    {
      "name": "Deserts",
      "title":
      "Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.  ",
      "desc":
      "Cras blandit consequat sapien ut cursus.Duis in mollis de magna. Sed sit amet nulla non ex velit.",
      "icon": "assets/Products/deserts.png",
      "stockOut": true
    },
    {
      "name": "Burgers",
      "title":
      "Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.  ",
      "desc":
      "Cras blandit consequat sapien ut cursus.Duis in mollis de magna. Sed sit amet nulla non ex velit.",
      "icon": "assets/Products/Burgers.png",
      "stockOut": true
    },
    {
      "name": "Salad",
      "title":
      "Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.  ",
      "desc":
      "Cras blandit consequat sapien ut cursus.Duis in mollis de magna. Sed sit amet nulla non ex velit.",
      "icon": "assets/Products/Salad.png",
      "stockOut": false
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 20),),
            Image.asset("assets/icons/restaurant.png"),
            SizedBox(height: size.convert(context, 5),),
            Text("“Le Casa” Cafe",
            style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 20)),),
            SizedBox(height: size.convert(context, 5),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child: Row(
                children: <Widget>[
                  Text("Burgers, Salads, Burritos, Sandwiches, Drinks.",
                  style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 5),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RichText(text:TextSpan(
                  children: [
                    TextSpan(
                      text: "4.8 (2902 ratings)",
                      style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                    )
                  ]
                )),
                SizedBox(width: 8),
                RichText(text:TextSpan(
                    children: [
                      TextSpan(
                        text: "read review",
                        style: TextStyle(
                            fontSize: size.convert(context, 11),
                          fontFamily: "RobotoCondensed-Light",
                          color: Colors.black,
                          decoration: TextDecoration.underline
                        ),
                      )
                    ]
                )),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Text("\$3.5",
                    style: styles.pageTitleStyle(fontSize: size.convert(context, 11)),),
                    Text("Delivery",
                      style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                  ],),
                  SizedBox(width: 10),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("37",
                        style: styles.pageTitleStyle(fontSize: size.convert(context, 11)),),
                      Text("Min",
                        style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                    ],),
                  SizedBox(width: 10),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("3.5",
                        style: styles.pageTitleStyle(fontSize: size.convert(context, 11)),),
                      Text("Mi/Km",
                        style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                    ],),
                  SizedBox(height: 10),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child:Row(
                children: <Widget>[
                  Image.asset("assets/icons/image.png"),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
              Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
                child:Text("Address: 02133, 23, Salem str., Boston, MA, USA Phone: (617) 397 8384",
                  style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child:Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Image.asset("assets/icons/calender.png"),
                          SizedBox(width: 5,),
                          Text("Monday - Friday",
                            style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                          ),
                        ],
                      ),
                      Text("09:00 - 22:00",
                        style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                      ),
                    ],
                  ),
                  SizedBox(height: 3,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Image.asset("assets/icons/calender.png"),
                          SizedBox(width: 5,),
                          Text("Weekends",
                            style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                          ),
                        ],
                      ),
                      Text("14:00 - 22-00",
                        style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Divider(),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child: Row(
                    children: <Widget>[
                      Text("Menu by Categories",
                        style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16)),
                      ),
                    ],
                  ),
            ),
            SizedBox(height: size.convert(context, 3),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text("Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.",
                      style: styles.slideparaStyle(fontSize: size.convert(context, 12)),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            categories(),
            SizedBox(height: size.convert(context, 20),),
          ],
        ),
      ),
    );
  }
  categories() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
      child: ListView.separated(
          physics: ScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return DottedBorder(
              color: Colors.grey,
              strokeWidth: 1,
              dashPattern: [8, 4],
              radius: Radius.circular(10),
              strokeCap: StrokeCap.round,
              borderType: BorderType.RRect,
              child:  Container(
                padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10),
                    vertical: size.convert(context, 10)),
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              foodCategories[index]["name"] ?? "",
                              style: styles.RobotoCondensedRegular(
                                  fontSize: size.convert(context, 15)),
                            ),
                          ],
                        ),
                        SizedBox(height: 5,),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RichText(
                                  text: TextSpan(
                                      text: foodCategories[index]["title"] ?? "",
                                      style: styles.slideparaStyle(
                                          fontSize: size.convert(context, 11)))),
                            )
                          ],
                        ),
                        SizedBox(height: size.convert(context, 8),),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: RichText(
                                  text: TextSpan(
                                      text: foodCategories[index]["title"] ?? "",
                                      style: styles.slideparaStyle(
                                          fontSize: size.convert(context, 11)))),
                            ),
                            SizedBox(width: size.convert(context, 30),),
                            circularImage(
                              h: size.convert(context, 80),
                              w: size.convert(context, 80),
                              imageUrl: foodCategories[index]["icon"],
                              isProduct: true,
                              radius: 5,
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Container(height: size.convert(context, 10),);
          },
          itemCount: foodCategories.length ?? 0),
    );
  }
}
