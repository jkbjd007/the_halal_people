
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';

class addCategory extends StatefulWidget {
  @override
  _addCategoryState createState() => _addCategoryState();
}

class _addCategoryState extends State<addCategory> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  bool isStockAvilable = true;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Add a New Category",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.convert(context, 20),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Text(
                    "Add a New Category",
                    style: styles.pageTitleStyle(
                        fontSize: size.convert(context, 25)),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Drive license number is needed if driver has registered a car. For bicycle it is not necessary.",
                      style: styles.slideparaStyle(
                          fontSize: size.convert(context, 13)),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: size.convert(context, 20),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Text("Name of Category",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            SizedBox(height: 3,),
            Container(child: customTextField(
              textFieldHeight: size.convert(context, 55),
              textFieldWidth: size.convertWidth(context, 340),
              hints: "Desert",
              radius: 5,
              bgColor: Colors.white,
              isWidth: true,
              fontFamily: "RobotoCondensed-Light",
              fontColor: Colors.black,
              fontSize: size.convert(context, 15),
            ),),
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Image.asset("assets/icons/uploadIcon.png"),
                  Text("Add image of Category",style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),),
                  InkWell(
                      onTap: (){
                        print("press delete icons");
                      },
                      child: Icon(IcoFontIcons.uiDelete,color: Colors.red,size: size.convert(context,12),))
                ],),),
            SizedBox(height: size.convert(context, 10),),
            DottedBorder(
              color:  Colors.grey,
              strokeWidth: 1,
              dashPattern: [8, 4],
              radius: Radius.circular(10),
              strokeCap: StrokeCap.round,
              borderType: BorderType.RRect,
              child: Container(
                width: size.convertWidth(context, 341),
                height: size.convert(context, 201),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: InkWell(
                  onTap: (){
                    _settingModalBottomSheet();
                  },
                  child: Center(child:  Container(
                    width: size.convert(context, 63),
                    height: size.convert(context, 63),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(500),
                    ),
                    child: Image.asset("assets/icons/cameraL.png"),
                  ),
                  ),
                ),
              ),
            ),
            SizedBox(height: size.convert(context, 10),),

            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Text("Small Desciription",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: DottedBorder(
                  color:  Colors.grey,
                  strokeWidth: 1,
                  dashPattern: [8, 4],
                  radius: Radius.circular(10),
                  strokeCap: StrokeCap.round,
                  borderType: BorderType.RRect,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 27,vertical: 15),
                    child: Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla a convallis mauris. ",
                      style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),
                    ),
                  )
              ),
            ),

            SizedBox(height: size.convert(context, 10),),

            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Text("Large Desciription",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: DottedBorder(
                  color:  Colors.grey,
                  strokeWidth: 1,
                  dashPattern: [8, 4],
                  radius: Radius.circular(10),
                  strokeCap: StrokeCap.round,
                  borderType: BorderType.RRect,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 27,vertical: 15),
                    child: Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla a convallis mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur lacus velit, id dignissim diam hendrerit in olor sit amet, consectetur.",
                      style: styles.pageTitleStyle(fontSize: size.convert(context, 12)),
                    ),
                  )
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("AVAILABLE IN Stock",
                    style: styles.fontRegular(fontSize: size.convert(context, 15)),),
                  CupertinoSwitch(
                    onChanged: (val){
                      setState(() {
                        isStockAvilable = val;
                      });
                    },
                    value: isStockAvilable,
                    activeColor: buttonColor,
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 22),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
              child: Row(children: <Widget>[
                swapButton(
                  buttonWidth: size.convertWidth(context, 165),
                  buttonHieght: size.convert(context, 55),
                  buttonColor: Color(0xffc8171d),
                  buttonText: "DELETE",
                  radius: 5,
                  borderColor: Colors.transparent,
                  circleBorderColor: Colors.transparent,
                  circleColor: Colors.transparent,
                  fontfamily: "RobotoMedium",
                  textColor: Colors.white,
                ),
                SizedBox(width: size.convertWidth(context, 8,)),
                swapButton(
                  buttonWidth: size.convertWidth(context, 165),
                  buttonHieght: size.convert(context, 55),
                  buttonColor: Color(0xff3b5998),
                  buttonText: "SAVE",
                  radius: 5,
                  borderColor: Colors.transparent,
                  circleBorderColor: Colors.transparent,
                  circleColor: Colors.transparent,
                  fontfamily: "RobotoMedium",
                  textColor: Colors.white,
                ),
              ],),
            ),
            SizedBox(height: size.convert(context, 22),),
          ],
        ),
      ),);
  }
  _settingModalBottomSheet() {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: size.convert(context, 90),
            color: Colors.white,
            padding: EdgeInsets.symmetric(
                horizontal: size.convert(context, 22),
                vertical: size.convert(context, 16)),
            child: Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    print("Take a Photo");
                    _onImageButtonPressed(ImageSource.camera);
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: size.convert(context, 10)),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "take A Photo",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    print("Upload from gallery");
                    Navigator.pop(context);
                    _onImageButtonPressed(ImageSource.gallery);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: size.convert(context, 10),
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: size.convert(context, 12),
                        ),
                        Expanded(
                          child: Container(
                            child: Text(
                              "Upload From Gallery",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "LatoRegular",
                                fontSize: size.convert(context, 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
  _onImageButtonPressed(ImageSource sourceFile) async {
    print("Some thing happen");
    try {
      var imageFile1 = await ImagePicker.pickImage(source: sourceFile);
      setState(() {
        //imageFile = imageFile1;
      });
    } catch (e) {
      print("Error " + e.toString());
    }
  }

}
