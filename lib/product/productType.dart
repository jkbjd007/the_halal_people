
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';

import 'addProduct.dart';

class productType extends StatefulWidget {
  @override
  _productTypeState createState() => _productTypeState();
}

class _productTypeState extends State<productType> {
  List items = [{"name":"Cacke “Marbella”","icons":"assets/Products/CackeMarbella.png","detail":"Cras blandit consequat sapien ut cursus.","price":"2.13","stockOut":false,"count":34},
{"name":"Chocomocco","icons":"assets/Products/Chocomocco.png","detail":"Cras blandit consequat sapien ut cursus.","price":"11.13","stockOut":true,"count":14},
{"name":"Cupcake “Choco-Cherry”","icons":"assets/Products/CupcakeChocoCherry.png","detail":"Cras blandit consequat sapien ut cursus.","price":"2.13","stockOut":true,"count":44},
{"name":"“Brawn” Cupcake","icons":"assets/Products/BrawnCupcake.png","detail":"Cras blandit consequat sapien ut cursus.","price":"15.13","stockOut":false,"count":21},
{"name":"Cupcake “Choco-Nuts”","icons":"assets/Products/CupcakeChocoNuts.png","detail":"Cras blandit consequat sapien ut cursus.","price":"12.13","stockOut":true,"count":4},
{"name":"Cupcake “Choco-Nuts”","icons":"assets/Products/CupcakeChocoNuts.png","detail":"Cras blandit consequat sapien ut cursus.","price":"23.13","stockOut":false,"count":51}];
  String selectCate;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.convert(context, 20),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Text(
                    "Add Products in Category",
                    style: styles.pageTitleStyle(
                        fontSize: size.convert(context, 25)),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Drive license number is needed if driver has registered a car. For bicycle it is not necessary.",
                      style: styles.slideparaStyle(
                          fontSize: size.convert(context, 13)),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: size.convert(context, 10),
            ),
            Divider(),
            SizedBox(
              height: size.convert(context, 20),
            ),
            swapButton(
              onClick: (){
                Navigator.push(context, PageTransition(
                  child: addProduct(),
                  type: PageTransitionType.rightToLeft
                ));
              },
              buttonColor: buttonColor,
              radius: 10,
              buttonWidth: size.convertWidth(context, 341),
              buttonHieght: size.convert(context, 55),
              borderColor: Colors.transparent,
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              buttonText: "ADD A NEW PRODUCT",
              fontfamily: "RobotoLight",
              textColor: Colors.white,
            ),
            SizedBox(
              height: size.convert(context, 20),
            ),
      Container(
        margin: EdgeInsets.symmetric(
            horizontal: size.convertWidth(context, 35)),
        child: Row(
          children: <Widget>[
            Text("Expiration Date",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
          ],
        ),),
      Container(
        child: DottedBorder(
          color:  Colors.grey.withOpacity(0.5),
          strokeWidth: 1,
          dashPattern: [8, 4],
          radius: Radius.circular(10),
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: Container(
            width: size.convertWidth(context, 341),
            height: size.convert(context, 58),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10)
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
              ),
              child: Center(
                child: DropdownButton(
                  underline: Container(),
                  style: styles.hintsStyle(),
//              focusColor: Colors.blue,
                  isExpanded: true,
                  icon: Icon(Icons.keyboard_arrow_down,color: hintsColor,),
                  hint: Text("Deserts",style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                  // Not necessary for Option 1
                  value: selectCate,
                  onChanged: (newValue) {
                    setState(() {
                      selectCate = newValue;
                    });
                  },

                  items: foodCategory.map((title) {
                    return DropdownMenuItem(
                      child: Text(title,style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                      value: title,
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
        )
      ),
            SizedBox(
              height: size.convert(context, 20),
            ),
            item(),
            SizedBox(
              height: size.convert(context, 20),
            ),
          ],
        ),
      ),);
  }

  item(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
      child: ListView.separated(
        itemBuilder: (BuildContext context, int index){
          return DottedBorder(
            color:  Colors.grey,
            strokeWidth: 1,
            dashPattern: [20, 6],
            radius: Radius.circular(10),
            strokeCap: StrokeCap.round,
            borderType: BorderType.RRect,
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(right: size.convertWidth(context, 10),
                        top: size.convert(context, 10),
                      left: size.convertWidth(context, 10),
                    ),
                    child:!(items[index]["stockOut"]) ?Container(child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
            children: <Widget>[
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                            child: Text("OUT OF STOCK",
                              style: styles.hintsStyle(
                                  fontSize: size.convert(context, 25),
                                  color: Color(0xffFF0000)
                              ),),


                    ),
                  ],
                ),
              ),

              Container(
                width: size.convertWidth(context, 50),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("",
                      style: styles.RobotoCondensedBold(fontSize: size.convert(context, 13),
                          color: Colors.red),),
                    SizedBox(height: size.convert(context, 10),),
                    CupertinoSwitch(
                      onChanged: (val){
                        setState(() {
                          items[index]["stockOut"] = val;
                        });
                      },
                      value: items[index]["stockOut"],
                      activeColor: buttonColor,
                    )
                  ],
                ),
              )
            ],
          ),):Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                circularImage(
                                  w: size.convert(context, 85),
                                  h: size.convert(context, 72),
                                  isProduct: true,
                                  assetImage: true,
                                  imageUrl: items[index]["icons"],
                                  radius: 5,
                                ),
                                SizedBox(width: size.convertWidth(context, 10),),
                                Container(
                                  //color: Colors.red,
                                  width: size.convertWidth(context, 130),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(items[index]["name"]??"some ths",
                                        style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 13)),),
                                      Text("Cras blandit consequat sapien ut cursus. ",
                                        style: styles.slideparaStyle(fontSize: size.convert(context, 11)),)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("\$${items[index]["price"]??"2.23"}",
                                  style: styles.RobotoCondensedBold(fontSize: size.convert(context, 13),
                                      color: Colors.red),),
                                SizedBox(height: size.convert(context, 10),),
                                CupertinoSwitch(
                                  onChanged: (val){
                                    setState(() {
                                      items[index]["stockOut"] = val;
                                    });
                                  },
                                  value: items[index]["stockOut"],
                                  activeColor: buttonColor,
                                )
                              ],
                            )
                          ],
                        ),

                  ),
                  SizedBox(height: size.convert(context, 10),),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: size.convert(context, 30),
                          decoration: BoxDecoration(
                            color: items[index]["stockOut"]? Colors.transparent : Color(0xff5f5f5f),
                              border: Border(
                                  top: BorderSide(
                                      width: 0.5,
                                      color: Colors.grey
                                  )
                              )
                          ),
                          width:size.convertWidth(context, 166),
                          child: Center(
                            child: Text("Solid:${items[index]["count"]}",
                             style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 14)),),
                          ),
                        ),
                     Container(height: size.convert(context, 30),
                     width: 0.5,
                     color: Colors.grey,),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(
                              width: 0.5,
                              color: Colors.grey
                            )
                          )
                        ),
                          height: size.convert(context, 30),
                          width:size.convertWidth(context, 166),
                          child: Center(child: Text("Edit",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 14)),)))
                    ],),
                  )
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index){
          return Container(height: size.convert(context, 10),);
        },
        itemCount: items.length??0,
        physics: ScrollPhysics(),
        shrinkWrap: true,
      ),
    );

  }
}
