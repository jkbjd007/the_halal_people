import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/product/addCategory.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';

class productCategories extends StatefulWidget {
  @override
  _productCategoriesState createState() => _productCategoriesState();
}

class _productCategoriesState extends State<productCategories> {
  List foodCategories = [
    {
      "name": "Deserts",
      "title":
          "Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.  ",
      "desc":
          "Cras blandit consequat sapien ut cursus.Duis in mollis de magna. Sed sit amet nulla non ex velit.",
      "icon": "assets/Products/deserts.png",
      "stockOut": true
    },
    {
      "name": "Burgers",
      "title":
          "Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.  ",
      "desc":
          "Cras blandit consequat sapien ut cursus.Duis in mollis de magna. Sed sit amet nulla non ex velit.",
      "icon": "assets/Products/Burgers.png",
      "stockOut": true
    },
    {
      "name": "Salad",
      "title":
          "Cras blandit consequat sapien ut cursus. Duis in mollis de magna. Sed sit amet nulla. Pellentesque non ex velit.  ",
      "desc":
          "Cras blandit consequat sapien ut cursus.Duis in mollis de magna. Sed sit amet nulla non ex velit.",
      "icon": "assets/Products/Salad.png",
      "stockOut": false
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.convert(context, 20),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Text(
                    "Add Food Categories",
                    style: styles.pageTitleStyle(
                        fontSize: size.convert(context, 25)),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 35)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Drive license number is needed if driver has registered a car. For bicycle it is not necessary.",
                      style: styles.slideparaStyle(
                          fontSize: size.convert(context, 13)),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: size.convert(context, 10),
            ),
            Divider(),
            SizedBox(
              height: size.convert(context, 20),
            ),
            swapButton(
              onClick: (){
                Navigator.push(context, PageTransition(child: addCategory(),
                type: PageTransitionType.rightToLeft));
              },
              buttonColor: buttonColor,
              radius: 10,
              buttonWidth: size.convertWidth(context, 341),
              buttonHieght: size.convert(context, 55),
              borderColor: Colors.transparent,
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              buttonText: "ADD A NEW CATEGORY",
              fontfamily: "RobotoLight",
              textColor: Colors.white,
            ),
            SizedBox(
              height: size.convert(context, 20),
            ),
            categories(),
            SizedBox(
              height: size.convert(context, 20),
            ),
          ],
        ),
      ),
    );
  }

  categories() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
      child: ListView.separated(
          physics: ScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return DottedBorder(
              color: Colors.grey,
              strokeWidth: 1,
              dashPattern: [8, 4],
              radius: Radius.circular(10),
              strokeCap: StrokeCap.round,
              borderType: BorderType.RRect,
              child: !(foodCategories[index]["stockOut"])? Container(
                padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10),
                    vertical: size.convert(context, 10)),
                child: Column(children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        foodCategories[index]["name"] ?? "",
                        style: styles.RobotoCondensedRegular(
                            fontSize: size.convert(context, 15)),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Edit",
                            style: styles.RobotoCondensedRegular(
                                fontSize: size.convert(context, 15)),
                          ),
                          SizedBox(
                            width: size.convertWidth(context, 7),
                          ),
                          CupertinoSwitch(
                            onChanged: (val) {
                              setState(() {
                                foodCategories[index]["stockOut"] = val;
                              });
                            },
                            value: foodCategories[index]["stockOut"],
                            activeColor: buttonColor,
                          ),
                        ],
                      )
                    ],
                  ),
                  Container(
                    height: size.convert(context, 100),
                    child: Center(
                      child: Text("OUT OF STOCK",
                  style: styles.hintsStyle(
                      fontSize: size.convert(context, 25),
                      color: Color(0xffFF0000)
                  ),),
                    ),)
                ],),
              ) : Container(
                padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10),
                vertical: size.convert(context, 10)),
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              foodCategories[index]["name"] ?? "",
                              style: styles.RobotoCondensedRegular(
                                  fontSize: size.convert(context, 15)),
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  "Edit",
                                  style: styles.RobotoCondensedRegular(
                                      fontSize: size.convert(context, 15)),
                                ),
                                SizedBox(
                                  width: size.convertWidth(context, 7),
                                ),
                                CupertinoSwitch(
                                  onChanged: (val) {
                                    setState(() {
                                      foodCategories[index]["stockOut"] = val;
                                    });
                                  },
                                  value: foodCategories[index]["stockOut"],
                                  activeColor: buttonColor,
                                ),
                              ],
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RichText(
                                  text: TextSpan(
                                      text: foodCategories[index]["title"] ?? "",
                                      style: styles.slideparaStyle(
                                          fontSize: size.convert(context, 11)))),
                            )
                          ],
                        ),
                        SizedBox(height: size.convert(context, 8),),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: RichText(
                                  text: TextSpan(
                                      text: foodCategories[index]["title"] ?? "",
                                      style: styles.slideparaStyle(
                                          fontSize: size.convert(context, 11)))),
                            ),
                            SizedBox(width: size.convert(context, 30),),
                            circularImage(
                              h: size.convert(context, 80),
                              w: size.convert(context, 80),
                              imageUrl: foodCategories[index]["icon"],
                              isProduct: true,
                              radius: 5,
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Container(height: size.convert(context, 10),);
          },
          itemCount: foodCategories.length ?? 0),
    );
  }
}
