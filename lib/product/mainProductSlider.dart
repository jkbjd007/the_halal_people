import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:the_halal_people/product/productCategories.dart';
import 'package:the_halal_people/product/productType.dart';
import 'package:the_halal_people/product/restaurantView.dart';
import 'package:the_halal_people/sale/invoiceDeline.dart';
import 'package:the_halal_people/sale/invoiceHistory.dart';
import 'package:the_halal_people/sale/orderHistory.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
class mainProductSlider extends StatefulWidget {
  @override
  _mainProductSliderState createState() => _mainProductSliderState();
}

class _mainProductSliderState extends State<mainProductSlider> {
  PageController _pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  int selectBody = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: <Widget>[
        SizedBox(height: size.convert(context, 20),),
        Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 15)),
          child: Row(
            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                onTap: (){
                  onTapped(0);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
                      vertical: size.convert(context, 2)),
                  decoration:BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: selectBody == 0 ? buttonColor :Colors.transparent,
                              width: 2
                          )
                      )
                  ),
                  child: Text("Product Categories",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),
                        color: selectBody == 0 ? buttonColor: Colors.black),),
                ),
              ),
              GestureDetector(
                onTap: (){
                  onTapped(1);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
                      vertical: size.convert(context, 2)),
                  decoration:BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: selectBody == 1 ? buttonColor :Colors.transparent,
                              width: 2
                          )
                      )
                  ),
                  child: Text("Product",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),
                        color: selectBody == 1 ? buttonColor: Colors.black),),
                ),
              ),
              GestureDetector(
                onTap: (){
                  onTapped(2);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
                      vertical: size.convert(context, 2)),
                  decoration:BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: selectBody == 2 ? buttonColor :Colors.transparent,
                              width: 2
                          )
                      )
                  ),
                  child: Text("Restaurant View",
                    style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),
                        color: selectBody == 2 ? buttonColor: Colors.black),),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            child: PageView(
              controller: _pageController,
              dragStartBehavior: DragStartBehavior.start,
              onPageChanged: (val){
                pageChanged(val);
                print("index is ${val}");
              },
              physics: ScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                productCategories(),
                productType(),
                restaurantView()
              ],
            ),),
        )
      ],),
    );
  }
  void pageChanged(int index) {
    setState(() {
      selectBody = index;
    });
  }
  void onTapped(int index) {
    setState(() {
      selectBody = index;
      _pageController.jumpToPage(index);
//      _pageController.animateToPage(index,
//          duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }
}