
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/changePassword.dart';
import 'package:the_halal_people/profile/personalnfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/support/onlineSupport.dart';
import 'package:the_halal_people/support/personalSupport.dart';

class shareEarn extends DrawerContent {

  @override
  _shareEarnState createState() => _shareEarnState();
}

class _shareEarnState extends State<shareEarn> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
          trailingIcon: InkWell(
              onTap: widget.onMenuPressed,
              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Share & Earn", style: styles.pageTitleStyle(),),
          leadingIcon: Image.asset("assets/icons/rightArrowIcon.png"),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/earnInfo.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Share & Earn \$10.00",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Share your code with friends to give them 2 free deliveries, valid for 14 days on orders above \$24.00. When they place their first order, you’ll get \$10.00 off products, valid for 14 days on orders above \$24.00.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Text("Share Your Invite Code",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 15)),),
            SizedBox(height: size.convert(context, 3),),
            customTextField(
              textFieldWidth: size.convertWidth(context, 301),
              textFieldHeight: size.convert(context, 55),
              bgColor: Colors.white,
            )

          ],),
      ),
    );
  }
}
