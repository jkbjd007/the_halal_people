import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:the_halal_people/sign/signAndSignUp.dart';
class sliders extends StatefulWidget {
  @override
  _slidersState createState() => _slidersState();
}

class _slidersState extends State<sliders> {
   CarouselController _controller = CarouselController();

   List<String> pagesName = ["A","B","C","D","E"];
   int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        CarouselSlider(
          carouselController: _controller,
            items: <Widget>[page1(),page2(),page3(),page4(),page5()],
            //carouselController: _controller,
            options: CarouselOptions(

              height: MediaQuery.of(context).size.height,
              autoPlay: false,
              enlargeCenterPage: true,
              viewportFraction: 1,
              initialPage: 0,

              //enableInfiniteScroll: false,
              scrollDirection: Axis.horizontal,
              onPageChanged: (index,reason){
                setState(() {
                  _current = index;
                });

                if(_current==4){
                  Navigator.pushReplacement(context, PageTransition(child: signAndSignUp(),type: PageTransitionType.upToDown));
                }
                print("${index}");
              }
            ),

        ),
//        Align(child: Container(
//          child:Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: pagesName.Map((num) {
//              int index = pages.indexOf(num);
//              return Container(
//                width: 8.0,
//                height: 8.0,
//                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
//                decoration: BoxDecoration(
//                  shape: BoxShape.circle,
//                  color: _current == index
//                      ? Color.fromRGBO(0, 0, 0, 0.9)
//                      : Color.fromRGBO(0, 0, 0, 0.4),
//                ),
//              );
//            }).toList(),
//        ),
//        )


        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.only(bottom: size.convert(context, 20)),
            child: swapButton(
              buttonColor: buttonColor,
              buttonWidth: size.convertWidth(context, 300),
              buttonHieght: size.convert(context, 55),
              circleHieght: size.convert(context, 55),
              circleWidth: size.convert(context, 55),
              icon: Image.asset("assets/icons/ic_right_arrow.png"),
              buttonText: "Create Account",
              textColor: Colors.white,
              onClick: (){
                if(_current<4){
                  setState(() {
                    _current ++;
                  });
                  _controller.nextPage(
                      duration: Duration(milliseconds: 300,),
                      curve: Curves.linear
                  );
                }
                else{
                  Navigator.pushReplacement(context, PageTransition(child: signAndSignUp(),type: PageTransitionType.upToDown));
                }

              },
            ),),),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.only(bottom: size.convert(context, 90),
            left: size.convertWidth(context, 160),
              right: size.convertWidth(context, 160),
            ),
          child: Row(children: pagesName.map((name){
            int index = pagesName.indexOf(name);
            print("${index} ${_current}");
            return Container(
              width: size.convert(context, 10),
              height: size.convert(context, 10),
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 1,
                color: buttonColor),
                color: _current == index
                    ? buttonColor
                    : Colors.white,
              ),
            );
          }).toList(),),
        ),)
      ],)
    );
  }

}

class page1 extends StatefulWidget {
  @override
  _page1State createState() => _page1State();
}

class _page1State extends State<page1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body(),);
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convertHeight(context, 40),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                 // color: Colors.red,
                  child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/backGroundIcon.png"),
                    Column(
                      children: <Widget>[
                        SizedBox(height: size.convert(context, 40),),
                        Image.asset("assets/icons/slide1Icon.png"),
                      ],
                    ),
                  ],
                ),)
              ],),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Add Home Based Business",style: styles.slideHeadingStyle(),),),
            SizedBox(height: size.convertHeight(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Praesent luctus accumsan malesuada. spendisse rutrum pretium consequat.\n \nPellentesque accumsan euismod tortor aliquam facilisis.In facilisis lacus leo, sit amet laoreet lectus facilisis quis.\n \nSed tristique dictum mi, sit amet dignissim erat malesuada et.",style: styles.slideparaStyle(),),),
          ],),
      ),
    );
  }
}

/////////////////////////Page 1 end//////////////////////////

class page2 extends StatefulWidget {
  @override
  _page2State createState() => _page2State();
}

class _page2State extends State<page2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body(),);
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convertHeight(context, 20),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/backGroundIcon.png"),
                    Column(
                      children: <Widget>[
                        SizedBox(height: size.convert(context, 90),),
                        Image.asset("assets/icons/slide2Icon.png"),
                      ],
                    ),
                  ],
                ),)
              ],),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Add Products",style: styles.slideHeadingStyle(),),),
            SizedBox(height: size.convertHeight(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Praesent luctus accumsan malesuada. spendisse rutrum pretium consequat.\n \nPellentesque accumsan euismod tortor aliquam facilisis.In facilisis lacus leo, sit amet laoreet lectus facilisis quis.\n \nSed tristique dictum mi, sit amet dignissim erat malesuada et.",style: styles.slideparaStyle(),),),
          ],),
      ),
    );
  }
}

/////////////////////////Page 2 end//////////////////////////

class page3 extends StatefulWidget {
  @override
  _page3State createState() => _page3State();
}

class _page3State extends State<page3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body(),);
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convertHeight(context, 0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/backGroundIcon.png"),
                    Column(
                      children: <Widget>[
                        SizedBox(height: size.convert(context, 90),),
                        Image.asset("assets/icons/slide3Icon.png"),
                      ],
                    ),
                  ],
                ),)
              ],),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Sell Your Product by Us",style: styles.slideHeadingStyle(),),),
            SizedBox(height: size.convertHeight(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Praesent luctus accumsan malesuada. spendisse rutrum pretium consequat.\n \nPellentesque accumsan euismod tortor aliquam facilisis.In facilisis lacus leo, sit amet laoreet lectus facilisis quis.\n \nSed tristique dictum mi, sit amet dignissim erat malesuada et.",style: styles.slideparaStyle(),),),
          ],),
      ),
    );
  }
}

/////////////////////////Page 3 end//////////////////////////

class page4 extends StatefulWidget {
  @override
  _page4State createState() => _page4State();
}

class _page4State extends State<page4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body(),);
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convertHeight(context, 0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/backGroundIcon.png"),
                    Column(
                      children: <Widget>[
                        SizedBox(height: size.convert(context, 90),),
                        Image.asset("assets/icons/slide4Icon.png"),
                      ],
                    ),
                  ],
                ),)
              ],),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Delivering Service",style: styles.slideHeadingStyle(),),),
            SizedBox(height: size.convertHeight(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Praesent luctus accumsan malesuada. spendisse rutrum pretium consequat.\n \nPellentesque accumsan euismod tortor aliquam facilisis.In facilisis lacus leo, sit amet laoreet lectus facilisis quis.\n \nSed tristique dictum mi, sit amet dignissim erat malesuada et.",style: styles.slideparaStyle(),),),
          ],),
      ),
    );
  }
}

/////////////////////////Page 4 end//////////////////////////

class page5 extends StatefulWidget {
  @override
  _page5State createState() => _page5State();
}

class _page5State extends State<page5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: body(),);
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.convertHeight(context, 0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset("assets/icons/backGroundIcon.png"),
                    Column(
                      children: <Widget>[
                        SizedBox(height: size.convert(context, 90),),
                        Image.asset("assets/icons/slide5Icon.png"),
                      ],
                    ),
                  ],
                ),)
              ],),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Get 24/7 Support",style: styles.slideHeadingStyle(),),),
            SizedBox(height: size.convertHeight(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convert(context, 28)),
              child: Text("Praesent luctus accumsan malesuada. spendisse rutrum pretium consequat.\n \nPellentesque accumsan euismod tortor aliquam facilisis.In facilisis lacus leo, sit amet laoreet lectus facilisis quis.\n \nSed tristique dictum mi, sit amet dignissim erat malesuada et.",style: styles.slideparaStyle(),),),
          ],),
      ),
    );
  }
}

/////////////////////////Page 5 end//////////////////////////




