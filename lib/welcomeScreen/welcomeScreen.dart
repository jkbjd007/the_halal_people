import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/welcomeScreen/sliders.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:page_transition/page_transition.dart';
class welcomeScreen extends StatefulWidget {
  @override
  _welcomeScreenState createState() => _welcomeScreenState();
}

class _welcomeScreenState extends State<welcomeScreen> {
  @override
  Widget build(BuildContext context) {
//    print("device Pixel = ${MediaQuery.of(context).devicePixelRatio}");
//    print("device Pixel = ${MediaQuery.of(context).physicalDepth}");
//    print("device Pixel = ${window.physicalSize.aspectRatio}");
    //ScreenUtil.init(context, width: 432, height: 816, allowFontScaling: false);
    return Scaffold(
        body: Container(

          ///margin: EdgeInsets.symmetric(horizontal: 120),
          child: Column(
            children: <Widget>[
              SizedBox(height: size.convertHeight(context, 160),),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Container(
                  child: Image.asset("assets/icons/logoHalal.jpg",
                    height: size.convert(context, 144),
                    width: size.convert(context, 144),
                  ),),
              ],),
              SizedBox(height: size.convertHeight(context, 55)),
              swapButton(
                buttonWidth: size.convertWidth(context, 231),
                buttonHieght: size.convert(context, 55),
                circleHieght: size.convert(context, 55),
                circleWidth: size.convert(context, 55),
                icon: Image.asset("assets/icons/ic_right_arrow.png"),
                buttonText: "Welcome",
                onClick: (){
                  Navigator.push(context, PageTransition(child: sliders(),
                  type: PageTransitionType.leftToRight));
                },
              )
            ],
          ),
        )
    );
  }
}
