import 'package:intl/intl.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/changePassword.dart';
import 'package:the_halal_people/profile/personalnfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

class schedule extends StatefulWidget {
  @override
  _scheduleState createState() => _scheduleState();
}

class _scheduleState extends State<schedule> {
  bool isShowTime = true;
  bool isEditTime = false;
  bool isCurrentDate = false;
  String startTime = "09:30";
  String endTime = "17:30";
  String seletedDate = "";
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return isEditTime ? editTime() : body();
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                children: <Widget>[
                  Text("Working Days Setting",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Drive license number is needed if driver has registered a car. For bicycle it is not necessary.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20)),
                height: size.convert(context, 320),
                child: CalendarCarousel(
                  headerTextStyle: styles.RobotoCondensedRegular(fontSize: size.convert(context, 15)),
                  weekdayTextStyle: styles.RobotoMedium(),
                  daysTextStyle: styles.fontRegular(fontSize: size.convert(context, 14)),
                  weekendTextStyle: styles.fontRegular(fontSize: size.convert(context, 14)),
                  leftButtonIcon: Image.asset("assets/icons/leftIcon.png"),
                  rightButtonIcon: Image.asset("assets/icons/rightIcon.png"),
                  selectedDayBorderColor: Colors.pink,
                  selectedDayButtonColor: Colors.tealAccent,
                  todayButtonColor: Color(0xfffafbfd),
                  todayBorderColor: Colors.black,
                  todayTextStyle: styles.fontRegular(fontSize: size.convert(context, 14)),
                  onDayPressed:(val,date){
                    DateFormat.yMMMMEEEEd().format(val);
                    seletedDate = DateFormat.MMMMEEEEd().format(val).toString() ;

                    //print("${(DateFormat.EEEE(DateTime.now())).toString()}");
                    print("this val = ${val}");
                    print("this date = ${date}");
                    print("this datee = ${seletedDate}");
                  },
                ) ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Divider(thickness: 1,),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Working Day",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),
                  Container(child: CupertinoSwitch(
                    activeColor: buttonColor,
                    onChanged: (val){
                      setState(() {
                        isShowTime = val;
                      });
                    },
                    value:isShowTime ,
                  ),)
                ],),
            ),
            isShowTime ? Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Column(
                children: <Widget>[
                  SizedBox(height: size.convert(context, 5),),
                  Divider(thickness: 1,),
                  SizedBox(height: size.convert(context, 5),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RichText(text: TextSpan(children: [
                        TextSpan(
                          text: startTime,
                          style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 18)),
                        ),
                        TextSpan(
                          text: " To ",
                          style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 18)),
                        ),
                        TextSpan(
                          text: endTime,
                          style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 18)),
                        ),
                      ]),),
                      InkWell(
                        onTap: (){
                          setState(() {
                            isEditTime = true;
                          });
                        },
                        child: Container(child: Row(children: <Widget>[
                          Icon(IcoFontIcons.edit,size: size.convert(context, 11),),
                          SizedBox(width: size.convertWidth(context, 5),),
                          Text("Edit",style: styles.pageTitleStyle(),),
                        ],),),
                      )
                    ],),
                  SizedBox(height: size.convert(context, 5),),
                ],
              ),
            ) : Container(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Divider(thickness: 1,),
            ),
            SizedBox(height: size.convert(context, 5),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(children: <Widget>[
                radioButton(
                  onchange: (val){
                    if(val){
                      setState(() {
                        isCurrentDate = val;
                      });
                    }
                  },
                  enable: isCurrentDate,
                ),
                SizedBox(width: size.convertWidth(context, 10),),
                Text("NOT AVAILABLE",style: styles.hintsStyle(fontSize: size.convert(context, 10)),),
                SizedBox(width: size.convertWidth(context, 25),),
                radioButton(
                  onchange: (val){
                    if(!val){
                      setState(() {
                        isCurrentDate = val;
                      });
                    }
                  },
                  enable: !isCurrentDate,
                ),
                SizedBox(width: size.convertWidth(context, 10),),
                Text("CURRENT DATE",style: styles.hintsStyle(fontSize: size.convert(context, 10)),),
              ],),
            )
          ],),
      ),
    );
  }
  editTime(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 37)),
      child: Column(children: <Widget>[
        SizedBox(height: size.convert(context, 20),),
        Row(
          children: <Widget>[
            Container(
              child: Text(seletedDate??"",style: styles.fontRegular(fontSize: size.convert(context, 18)),),),
          ],
        ),
        SizedBox(height: size.convert(context, 10),),
        Divider(thickness: 1,),
        Container(
          margin: EdgeInsets.symmetric(vertical: size.convert(context, 10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Text("Start Time",style: styles.pageTitleStyle(fontSize: size.convert(context, 15)),),
            InkWell(
                onTap: (){
                  _datePickerLocalized(start: true);
                },
                child: Text(startTime??"",style: styles.pageTitleStyle(fontSize: size.convert(context, 15)),))
          ],),
        ),
        Divider(thickness: 1,),
        Container(
          margin: EdgeInsets.symmetric(vertical: size.convert(context, 10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("End Time",style: styles.pageTitleStyle(fontSize: size.convert(context, 15)),),
              InkWell(
                onTap: (){
                  _datePickerLocalized(start: false);
                },
                  child: Text(endTime??"",style: styles.pageTitleStyle(fontSize: size.convert(context, 15)),))
            ],),
        ),
        Divider(thickness: 1,),
        SizedBox(height: size.convert(context, 20),),
        swapButton(
          onClick: (){
            setState(() {
              isEditTime = false;
            });
          },
          buttonWidth: size.convertWidth(context, 301),
          buttonHieght: size.convert(context, 55),
          textColor: Colors.white,
          buttonText: "SAVE",
          circleBorderColor: Colors.transparent,
          circleColor: Colors.transparent,
          buttonColor: Colors.black,
        ),
      ],),
    );
  }

  _datePickerLocalized({bool start = true}) {
    return showModalBottomSheet(
      backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: size.convert(context, 215),
              child:Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35),
                        vertical: size.convert(context, 10)),
                        child: InkWell(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Done",style: styles.fontRegular(fontSize: size.convert(context, 15)),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: CupertinoDatePicker(
                      use24hFormat: false,
                      onDateTimeChanged: (val){
                        setState(() {
                          if(start) startTime = (val.toString()).substring(11,16);
                          else endTime = (val.toString()).substring(11,16);
                        });
                      },
                      mode: CupertinoDatePickerMode.time,
                    ),
                  ),
                ],
              )
          );
        });
  }


}
