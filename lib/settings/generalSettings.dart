import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/profile/changePassword.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/settings/privacyPolicy.dart';
import 'package:the_halal_people/settings/useTerm.dart';
class generalSettings extends DrawerContent {
  @override
  _generalSettingsState createState() => _generalSettingsState();
}

class _generalSettingsState extends State<generalSettings> {
  bool shareLocation = true;
  bool doCall = false;
  bool notification = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
          trailingIcon: InkWell(
              onTap: widget.onMenuPressed,
              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Settings", style: styles.pageTitleStyle(),),
          leadingIcon: Image.asset("assets/icons/rightArrowIcon.png"),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                children: <Widget>[
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            text:"General Settings",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),
                          )
                        ]
                    ),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 5),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text:"Drive license number is needed if driver has registered a car. For bicycle it is not necessary."
                              ,style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 30),),

            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Text("Notifications",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),
                CupertinoSwitch(
                  onChanged: (val){
                    setState(() {
                      notification = val;
                    });
                  },
                  value: notification,
                  activeColor: buttonColor,
                ),
              ],),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Divider(),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, PageTransition(child: changePassword(),
                  type: PageTransitionType.rightToLeft));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Change Password",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),

                  ],),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Divider(),
            ),

            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Do Not Call",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),
                  CupertinoSwitch(
                    onChanged: (val){
                      setState(() {
                        doCall = val;
                      });
                    },
                    value: doCall,
                    activeColor: buttonColor,
                  ),
                ],),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Divider(),
            ),

            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("I Agree To Share My Location",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),
                  CupertinoSwitch(
                    onChanged: (val){
                      setState(() {
                        shareLocation = val;
                      });
                    },
                    value: shareLocation,
                    activeColor: buttonColor,
                  ),
                ],),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Divider(),
            ),

            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, PageTransition(child: useTerms(),
                  type: PageTransitionType.rightToLeft));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Terms of Use",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),

                  ],),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Divider(),
            ),

            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: InkWell(
                onTap: (){
                  Navigator.push(context, PageTransition(child: privacyPolicy(),
                      type: PageTransitionType.rightToLeft));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Privacy Policy",style: styles.pageTitleStyle(fontSize: size.convert(context, 18)),),

                  ],),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Divider(),
            ),
          ],),),);
  }
}