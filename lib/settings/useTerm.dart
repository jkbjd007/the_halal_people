import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
class useTerms extends DrawerContent {
  @override
  _useTermsState createState() => _useTermsState();
}

class _useTermsState extends State<useTerms> {

  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Use Terms", style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                children: <Widget>[
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        children: [
                          TextSpan(
                            text:"Terms os Use",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),
                          )
                        ]
                    ),),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 5),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 35)),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text:"Nulla cursus vitae nisi sed venenatis. Fusce purus eros, imperdiet at mattis vel, fringilla a odio. Vestibulum luctus in lorem a mattis. Sed euismod tincidunt orci, vitae faucibus ligula scelerisque nec. Sed a orci quis lorem maximus accumsan lacinia in nisi. \n\n"
                                  "Cras blandit consequat sapien ut cursus. Duis in mollis magna. Sed sit amet faucibus nulla. Pellentesque non ex velit\n\n"
                                  "Nam a nisi eu arcu dictum mattis. Aenean varius justo a sollicitudin tincidunt. Nullam congue sed dolor ut vehicula. Sed lobortis et nisl id auctor. Nulla cursus vitae nisi sed venenatis. Fusce purus eros, imperdiet at mattis vel, fringilla a odio.  Vestibulum luctus in lorem a mattis. Sed euismod tincidunt orci, vitae faucibus ligula scelerisque nec. Sed a orci quis lorem maximus accumsan lacinia in nisi. \n\n"
                                  "Cras blandit consequat sapien ut cursus. Duis in mollis magna. Sed sit amet faucibus nulla. Pellentesque non ex velit. \n\n"
                                  "Nam a nisi eu arcu dictum mattis. Aenean varius justo a sollicitudin tincidunt. Nullam congue sed dolor ut vehicula. Sed lobortis et nisl id auctor.\n\n"
                                  "Nulla cursus vitae nisi sed venenatis. Fusce purus eros, imperdiet at mattis vel, fringilla a odio. Vestibulum luctus in lorem a mattis. Sed euismod tincidunt orci, vitae faucibus ligula scelerisque nec. Sed a orci quis lorem maximus accumsan lacinia in nisi. \n\n"
                                  "Cras blandit consequat sapien ut cursus. Duis in mollis magna. Sed sit amet faucibus nulla. Pellentesque non ex velit. \n\n"
                                  "Nam a nisi eu arcu dictum mattis. Aenean varius justo a sollicitudin tincidunt. Nullam congue sed dolor ut vehicula. Sed lobortis et nisl id auctor."
                              ,style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  ),
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),

          ],),),);
  }
}