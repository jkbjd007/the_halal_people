import 'package:flutter/material.dart';
import 'package:the_halal_people/res/colors.dart';
class styles{
  static TextStyle slideHeadingStyle ({double fontSize = 23 , String fontFamily = "RobotoLight", Color  color = Colors.black}){
    return TextStyle(
    fontFamily: fontFamily,
    fontSize: fontSize,
color: color
);
}

  static TextStyle slideparaStyle ({double fontSize = 14 , String fontFamily = "RobotoThin", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle fontRegular({double fontSize = 17 , String fontFamily = "RobotoRegular", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }

  static TextStyle pageTitleStyle({double fontSize = 14 , String fontFamily = "RobotoCondensed-Light", Color  color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle hintsStyle({double fontSize = 15 , String fontFamily = "RobotoLight",Color color}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color??hintsColor
    );
  }
  static TextStyle RobotoCondensedRegular({double fontSize = 18 , String fontFamily = "RobotoCondensed-Regular", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle RobotoCondensedBold({double fontSize = 18 , String fontFamily = "RobotoCondensed-Boldr", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
  static TextStyle RobotoMedium({double fontSize = 10 , String fontFamily = "RobotoMedium", Color color = Colors.black}){
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        color:color
    );
  }
}

