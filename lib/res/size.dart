import 'package:flutter/cupertino.dart';
import 'dart:ui';

class size{
  static convert(BuildContext context,double n){
    return (MediaQuery.of(context).size.longestSide/683)*(n);
  }
  static convertWidth(BuildContext context,double n){
    return (MediaQuery.of(context).size.width/411)*(n);
  }
  static convertHeight(BuildContext context,double n){
    return (MediaQuery.of(context).size.height/683)*(n);
  }
}