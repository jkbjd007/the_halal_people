import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/sign/resetPassword.dart';
class otpVerification extends StatefulWidget {
  @override
  _otpVerificationState createState() => _otpVerificationState();
}

class _otpVerificationState extends State<otpVerification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(
      child: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(height: size.convert(context, 100),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 132
            )),
            child: Image.asset("assets/icons/logoHalal.jpg",
              height: size.convert(context, 110),),
            width: size.convert(context, 110),
          ),
          SizedBox(height: size.convert(context, 15),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 67)),
            child:Text("Verification codes OTP",
              style: styles.slideparaStyle(fontSize: 15,fontFamily: "RobotoRegular"),
              textAlign: TextAlign.center,
              textScaleFactor: 1,
            ),),
          SizedBox(height: size.convert(context, 10),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 67)),
            child:Text("Curabitur sit amet massa nunc. Fusce at tristique magna. Fusce eget dapibus dui.",
              style: styles.slideparaStyle(fontSize: 12),
              textAlign: TextAlign.center,
              textScaleFactor: 1,
            ),),
          SizedBox(height: size.convert(context, 7),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 67)),
            child:Text("+1 (617) 397-8483",
              style: styles.slideparaStyle(fontSize: 12),
              textAlign: TextAlign.center,
              textScaleFactor: 1,
            ),),
          SizedBox(height: size.convert(context, 30),),
          DottedBorder(
            color:  Colors.grey,
            strokeWidth: 1,
            dashPattern: [8, 4],
            radius: Radius.circular( 50),
            strokeCap: StrokeCap.round,
            borderType: BorderType.RRect,
            child: Container(
              width: size.convertWidth(context, 301),
              height: size.convert(context, 55),
              decoration: BoxDecoration(
                  color: Color(0xfff2f2f2).withOpacity(0.3),
                  borderRadius: BorderRadius.circular(50)
              ),
              child: Center(
                child: OTPTextField(
                  length: 5,
                  width: size.convertWidth(context, 180),
                  fieldWidth: size.convertWidth(context, 10),
                  style: TextStyle(
                      fontSize: 17
                  ),
                  textFieldAlignment: MainAxisAlignment.spaceAround,
                  fieldStyle: FieldStyle.underline,
                  onCompleted: (pin) {
                    print("Completed: " + pin);
                  },
                ),
              ),

            ),
          ),
          SizedBox(height: 10,),
          swapButton(
            onClick: (){
              Navigator.pushReplacement(context, PageTransition(child: resetPassword(),type: PageTransitionType.upToDown));
            },
            buttonWidth: size.convertWidth(context, 301),
            buttonHieght: size.convert(context, 58),
            buttonText: "LOGIN",
            circleColor: Colors.transparent,
            buttonColor: buttonColor,
            textColor: Colors.white,
            circleBorderColor: Colors.transparent,
          ),

        ],),
      ),

    ));
  }
}
