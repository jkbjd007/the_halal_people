import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/sign/otpVerification.dart';
class editPhoneNumber extends StatefulWidget {
  @override
  _editPhoneNumberState createState() => _editPhoneNumberState();
}

class _editPhoneNumberState extends State<editPhoneNumber> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(
      child: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(height: size.convert(context, 100),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 132
            )),
            child: Image.asset("assets/icons/logoHalal.jpg",
              height: size.convert(context, 110),),
            width: size.convert(context, 110),
          ),
          SizedBox(height: size.convert(context, 15),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 67)),
            child:Text("Curabitur sit amet massa nunc. Fusce at tristique magna. Fusce eget dapibus dui.",
              style: styles.slideparaStyle(fontSize: 12),
              textAlign: TextAlign.center,
              textScaleFactor: 1,
            ),),
          SizedBox(height: size.convert(context, 30),),
          customTextField(
            textFieldWidth: size.convertWidth(context, 301),
            textFieldHeight: size.convert(context, 55),
            hints: "+995 599771803",
            fontSize: size.convert(context, 15),
            fontFamily: "RobotoLight",
            fontColor: Colors.black.withOpacity(0.35),
            icons: Container(
              child: Row(children: <Widget>[
                SizedBox(width: 10,),
                Image.asset("assets/icons/flag_usa.png"),
                SizedBox(width: 10,),
                Icon(Icons.keyboard_arrow_down),
                SizedBox(width: 10,),
              ],),
            ),
          ),
          SizedBox(height: 10,),
          swapButton(
            onClick: (){
              Navigator.pushReplacement(context, PageTransition(child: otpVerification(),type: PageTransitionType.upToDown));
            },
            buttonWidth: size.convertWidth(context, 301),
            buttonHieght: size.convert(context, 58),
            buttonText: "LOGIN",
            circleColor: Colors.transparent,
            buttonColor: buttonColor,
            textColor: Colors.white,
            circleBorderColor: Colors.transparent,
          ),

        ],),
      ),

    ));
  }
}
