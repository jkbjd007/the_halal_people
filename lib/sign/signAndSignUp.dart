import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/sign/editPhoneNumber.dart';
class signAndSignUp extends StatefulWidget {
  @override
  _signAndSignUpState createState() => _signAndSignUpState();
}

class _signAndSignUpState extends State<signAndSignUp> {
  @override
  bool isLoginBody = false;
  Widget build(BuildContext context) {
    return Scaffold(body: Container(

      child: SingleChildScrollView(child:
        Column(children: <Widget>[
          SizedBox(height: size.convert(context, 100),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 132
            )),
            child: Image.asset("assets/icons/logoHalal.jpg",
          height: size.convert(context, 110),),
            width: size.convert(context, 110),
          ),
          SizedBox(height: size.convert(context, 15),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 67)),
            child:Text("Curabitur sit amet massa nunc. Fusce at tristique magna. Fusce eget dapibus dui.",
          style: styles.slideparaStyle(fontSize: 12),
            textAlign: TextAlign.center,
              textScaleFactor: 1,
            ),),
          SizedBox(height: size.convert(context, 10),),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            GestureDetector(
              onTap: (){
                setState(() {
                  isLoginBody = true;
                });
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35),
                vertical: size.convert(context, 10)),
                decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 2,
                        color: isLoginBody ? Colors.black : Colors.black.withOpacity(0.3),
                      ),
                    )
                ),
                child: Text("LOGIN",
                style: TextStyle(
                  fontSize: size.convert(context, 20),
                  color: isLoginBody ? Colors.black : Colors.black.withOpacity(0.3),
                  fontFamily:"RobotoMedium",
                ),
              ),),
            ),
            GestureDetector(
              onTap: (){
                setState(() {
                  isLoginBody = false;
                });
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 35),
                    vertical: size.convert(context, 10)),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 2,
                      color: !isLoginBody ? Colors.black : Colors.black.withOpacity(0.3),
                    ),
                  )
                ),
                child: Text("SIGN UP",
                style: TextStyle(
                    fontSize: size.convert(context, 20),
                    color: !isLoginBody ? Colors.black : Colors.black.withOpacity(0.3),
                  fontFamily:"RobotoMedium",
                ),
              ),),
            ),
          ],),),
          SizedBox(height: size.convert(context, 10),),
          isLoginBody ? sginInBody() : sginUpBody(),
        ],),),
    ));
  }
  sginInBody(){
    return Container(
      child: Column(children: <Widget>[
        customTextField(
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "+995 599771803",
          fontSize: size.convert(context, 15),
          fontFamily: "RobotoLight",
          fontColor: Colors.black.withOpacity(0.35),
          icons: Container(
            child: Row(children: <Widget>[
              SizedBox(width: 10,),
              Image.asset("assets/icons/flag_usa.png"),
              SizedBox(width: 10,),
              Icon(Icons.keyboard_arrow_down),
              SizedBox(width: 10,),
            ],),
          ),
        ),
        SizedBox(height: 10,),
        customTextField(
          textFieldWidth: size.convertWidth(context, 301),
          textFieldHeight: size.convert(context, 55),
          hints: "Password",
          fontSize: size.convert(context, 15),
          fontFamily: "RobotoLight",
          fontColor: Colors.black.withOpacity(0.35),
          icons: Container(
            child: Row(children: <Widget>[
              SizedBox(width: 15,),
              Image.asset("assets/icons/lockIcon.png"),
              SizedBox(width: 30,),
            ],),
          ),
        ),
        SizedBox(height: 10,),
        swapButton(
          buttonWidth: size.convertWidth(context, 301),
          buttonHieght: size.convert(context, 58),
          buttonText: "SIGN IN",
          circleColor: Colors.transparent,
          buttonColor: buttonColor,
          textColor: Colors.white,
          circleBorderColor: Colors.transparent,
        ),
        SizedBox(height: 15,),
        InkWell(
          onTap: (){
            Navigator.pushReplacement(context, PageTransition(child: editPhoneNumber(),type: PageTransitionType.upToDown));
          },
          child: Container(child: Text("Forgot Password?",style: styles.slideHeadingStyle(fontSize: size.convert(context, 15),
          color: Color3333),),),
        ),
        SizedBox(height: size.convert(context, 100),),
        Container(child: RichText(text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "Do not have account?",
                style: styles.slideHeadingStyle(fontSize: size.convert(context, 15),
                    color: Color3333),
              ),
              TextSpan(
                text: "   Sign up",
                style: styles.slideHeadingStyle(fontSize: size.convert(context, 15),
                    color: Color3333,fontFamily: "RobotoRegular"),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    // single tapped
                    setState(() {
                      isLoginBody = false;
                      print("move to sign Up Page......");
                    });
                  },
              ),
            ]
        ),),),
      ],),
    );
  }
  sginUpBody(){
    return Container(child: Column(children: <Widget>[
      customTextField(
        textFieldWidth: size.convertWidth(context, 301),
        textFieldHeight: size.convert(context, 55),
        hints: "+995 599771803",
        fontSize: size.convert(context, 15),
        fontFamily: "RobotoLight",
        fontColor: Colors.black,
        icons: Container(
          child: Row(children: <Widget>[
            SizedBox(width: 15,),
            Image.asset("assets/icons/flag_usa.png"),
            SizedBox(width: 10,),
            Icon(Icons.keyboard_arrow_down),
            SizedBox(width: 10,),
          ],),
        ),
      ),
      SizedBox(height: 10,),
      customTextField(
        textFieldWidth: size.convertWidth(context, 301),
        textFieldHeight: size.convert(context, 55),
        hints: "Password",
        fontSize: size.convert(context, 15),
        fontFamily: "RobotoLight",
        fontColor: Colors.black.withOpacity(0.35),
        icons: Container(
          child: Row(children: <Widget>[
            SizedBox(width: 15,),
            Image.asset("assets/icons/lockIcon.png"),
            SizedBox(width: 30,),
          ],),
        ),
      ),
      SizedBox(height: 10,),
      customTextField(
        textFieldWidth: size.convertWidth(context, 301),
        textFieldHeight: size.convert(context, 55),
        hints: "Confirm Password",
        fontSize: size.convert(context, 15),
        fontFamily: "RobotoLight",
        fontColor: Colors.black.withOpacity(0.35),
        icons: Container(
          child: Row(children: <Widget>[
            SizedBox(width: 15,),
            Image.asset("assets/icons/lockIcon.png"),
            SizedBox(width: 30,),
          ],),
        ),
      ),
      SizedBox(height: 10,),
      customTextField(
        textFieldWidth: size.convertWidth(context, 301),
        textFieldHeight: size.convert(context, 55),
        hints: "Partner Code (not required)",
        fontSize: size.convert(context, 15),
        fontFamily: "RobotoLight",
        fontColor: Colors.black.withOpacity(0.35),
        icons: Container(
          child: Row(children: <Widget>[
            SizedBox(width: 15,),
            Image.asset("assets/icons/partnerIcon.png"),
            SizedBox(width: 30,),
          ],),
        ),
      ),
      SizedBox(height: 10,),
      swapButton(
        buttonWidth: size.convertWidth(context, 301),
        buttonHieght: size.convert(context, 58),
        buttonText: "SIGN IN",
        circleColor: Colors.transparent,
        buttonColor: buttonColor,
        textColor: Colors.white,
        circleBorderColor: Colors.transparent,
      ),

      SizedBox(height: size.convert(context, 15),),
      Container(child: RichText(text: TextSpan(
          children: <TextSpan>[
            TextSpan(
              text: "Already have account?",
              style: styles.slideHeadingStyle(fontSize: size.convert(context, 15),
                  color: Color3333),
            ),
            TextSpan(
              text: "   Login",
              style: styles.slideHeadingStyle(fontSize: size.convert(context, 15),
                  color: Color3333,fontFamily: "RobotoRegular"),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  // single tapped
                  setState(() {
                    isLoginBody = false;
                    print("move to sign Up Page......");
                  });
                },
            ),
          ]
      ),),),
    ],),);
  }
}
