import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/mainMenu.dart';
import 'package:the_halal_people/paymentMethod/paymentInfo.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/profileInfo.dart';
import 'package:the_halal_people/promoCode/promoCode.dart';
import 'package:the_halal_people/repeatedWidgeds/circularImage.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/settings/generalSettings.dart';
import 'package:the_halal_people/shareEarn/shareEarn.dart';
import 'package:the_halal_people/support/supportInfo.dart';

class MainPage extends DrawerContent {
  MainPage({Key key, this.title});
  final String title;
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(32.0)),
                  child: Material(
                    shadowColor: Colors.transparent,
                    color: Colors.transparent,
                    child: IconButton(
                      icon: Icon(
                        Icons.menu,
                        color: Colors.black,
                      ),
                      onPressed: widget.onMenuPressed,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(widget.title??""),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MainWidget extends StatefulWidget {
  MainWidget({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> with TickerProviderStateMixin {
  HiddenDrawerController _drawerController;

  @override
  void initState() {
    super.initState();
    _drawerController = HiddenDrawerController(
      initialPage: mainMenu(),
      items: [
//        DrawerItem(
//          page: addCompanyInfo(),
//        ),
        DrawerItem(
          text: 'My Products',
          icon: Image.asset("assets/icons/myProfile.png"),
          page: mainMenu(),
        ),
        DrawerItem(
          text: 'Profile',
          icon: Image.asset("assets/icons/profile.png"),
          page: mainMenu(),
        ),
        DrawerItem(
          text: 'Promocodes',
          icon: Image.asset("assets/icons/promocodes.png"),
          page: promoCode(),
        ),
        DrawerItem(
          text: 'Sales',
          icon: Image.asset("assets/icons/sale.png"),
          page: mainMenu(pageNumber: 5,),
        ),
        DrawerItem(
          text: 'Payment Methods',
          icon: Image.asset("assets/icons/paymentMethod.png"),
          page: paymentInfo(),
        ),
        DrawerItem(
          text: 'Share & Earn',
          icon: Image.asset("assets/icons/ic_earn.png"),
          page: shareEarn(),
        ),
        DrawerItem(
          text: 'Settings',
          icon: Image.asset("assets/icons/setting.png"),
          page: generalSettings(),
        ),
        DrawerItem(
          text: 'Support',
          icon: Image.asset("assets/icons/support.png"),
          page: supportInfo(),
        ),
        DrawerItem(
          text: 'Language',
          icon: Image.asset("assets/icons/flag_usa.png",width: 18,
          height: 18,),
          page: MainPage(
            title: 'SETTINGS',
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HiddenDrawer(
        controller: _drawerController,
        header: Container(
          margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 36)),
          alignment: Alignment.topLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                // height: 75,
                child: circularImage(
                  w: size.convert(context, 51),
                  h: size.convert(context, 51),
                  imageUrl: "assets/icons/Mask.png",
                ),
              ),
              SizedBox(
                height: size.convert(context, 6),
              ),
              Text(
                'Welcome',
                style: styles.slideHeadingStyle(fontSize: size.convert(context, 10),color: Colors.white),
              ),
              Text(
                'Monicas’ Trattoria',
                style: styles.fontRegular(fontSize: size.convert(context, 17),color: Colors.white),
              ),
              Text(
                '+1 (917) 470-9281',
                style: styles.slideHeadingStyle(fontSize: size.convert(context, 12),color: Colors.white),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [buttonColor, buttonColor, buttonColor],
            // tileMode: TileMode.repeated,
          ),
        ),
      ),
    );
  }
}