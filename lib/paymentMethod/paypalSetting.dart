import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/paymentMethod/addPaypalCard.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
class paypalSetting extends StatefulWidget {
  @override
  _paypalSettingState createState() => _paypalSettingState();
}

class _paypalSettingState extends State<paypalSetting> {
  bool radio1 = true;
  bool radio2 = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
      SizedBox(height: size.convert(context, 50),),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/icons/PayPalL.png"),
        ],
      ),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Add PayPal Address",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Add your paypal email address or add new one. This need for product delivery.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
              Row(children: <Widget>[
                Image.asset("assets/icons/icon_paypal.png"),
                SizedBox(width: size.convertWidth(context, 20)),
                Text("paypal@mydomain.net",style: styles.slideHeadingStyle(fontSize: size.convert(context, 15)),)
              ],),
              Container(child: radioButton(
                enable: radio1,
                onchange: (val){
                  if(val){
                    setState(() {
                      radio1 = val;
                      radio2 = false;
                    });
                  }
                },
              ),),
            ],),),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    Image.asset("assets/icons/icon_paypal.png"),
                    SizedBox(width: size.convertWidth(context, 20)),
                    Text("paypal@mydomain.net",style: styles.slideHeadingStyle(fontSize: size.convert(context, 15)),)
                  ],),
                  Container(child: radioButton(
                    enable: radio2,
                    onchange: (val){
                      if(val){
                        setState(() {
                        radio2 = val;
                        radio1 = false;
                        });
                      }
                    },
                  ),),
                ],),),
            SizedBox(height: size.convert(context, 40),),
            swapButton(
              onClick: (){
                Navigator.push(context, PageTransition(child: addPaypalCard(),type: PageTransitionType.leftToRight));
              },
              borderColor: Colors.transparent,
              buttonWidth: size.convertWidth(context, 301),
              buttonHieght: size.convertHeight(context, 58),
              buttonText: "ADD NEW ADRESS",
              buttonColor: buttonColor,
              textColor: Colors.white,
              circleColor: Colors.white,
              circleHieght: size.convert(context, 58),
              circleWidth: size.convert(context, 58),
              icon: Image.asset("assets/icons/plus.png"),
            ),
            SizedBox(height: size.convert(context, 10),),
            swapButton(
              borderColor: Colors.transparent,
              buttonWidth: size.convertWidth(context, 301),
              buttonHieght: size.convertHeight(context, 58),
              buttonText: "SAVE AS PRIMARY",
              buttonColor: Color(0xff3b5998),
              textColor: Colors.white,
              circleColor: Colors.transparent,
              circleBorderColor: Colors.transparent,
            ),
    ],),),);
  }
}
