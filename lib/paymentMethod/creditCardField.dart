import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
class creditCardField extends StatefulWidget {
  String hint;
  String lastHint;
  double fontSize;
  creditCardField({this.hint,this.lastHint,this.fontSize});
  @override
  _creditCardFieldState createState() => _creditCardFieldState();
}

class _creditCardFieldState extends State<creditCardField> {
  FocusNode myFocusNode1;
  FocusNode myFocusNode2;
  FocusNode myFocusNode3;
  FocusNode myFocusNode4;
  FocusNode myFocusNode5;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myFocusNode1 = FocusNode();
    myFocusNode2 = FocusNode();
    myFocusNode3 = FocusNode();
    myFocusNode4 = FocusNode();
    myFocusNode5 = FocusNode();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    myFocusNode1.dispose();
    myFocusNode2.dispose();
    myFocusNode3.dispose();
    myFocusNode4.dispose();
    myFocusNode5.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      color:  Colors.grey.withOpacity(0.5),
        strokeWidth: 1,
        dashPattern: [8, 4],
        radius: Radius.circular(10),
        strokeCap: StrokeCap.round,
        borderType: BorderType.RRect,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20),vertical: size.convert(context, 10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(children: <Widget>[
                Container(width : size.convertWidth(context, 40),
                    child: TextFormField(
                  maxLength: 4,
                  onChanged: (val){
                    if(val.length==4){
                      setState(() {
                        FocusScope.of(context).requestFocus(myFocusNode2);
                      });
                    }
                  },
                  decoration: InputDecoration(
                      counterText: '',
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: widget.hint==null ? "****": widget.hint,
                    hintStyle: styles.hintsStyle(),
                  ),
                  style: TextStyle(
                      fontSize: widget.fontSize ?? 16,
                      fontFamily: "RobotoLight",
                      color: Colors.black
                  ),
                  focusNode:  myFocusNode1,
                )),
                SizedBox(width: size.convertWidth(context, 6),),
                Container(width : size.convertWidth(context, 40),child: TextFormField(
                  maxLength: 4,
                  onChanged: (val){
                    if(val.length==4){
                      setState(() {
                        FocusScope.of(context).requestFocus(myFocusNode3);

                      });
                    }
                    if(val.length == 0){
                      FocusScope.of(context).requestFocus(myFocusNode1);
                    }
                  },
                  decoration: InputDecoration(
                    counterText: '',
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: widget.hint==null ? "****": widget.hint,
                    hintStyle: styles.hintsStyle(),
                  ),
                  style: TextStyle(
                      fontSize: widget.fontSize ?? 16,
                      fontFamily: "RobotoLight",
                      color: Colors.black
                  ),
                  focusNode:  myFocusNode2,
                )),
                SizedBox(width: size.convertWidth(context, 6),),
                Container(width : size.convertWidth(context, 40),child: TextFormField(
                  maxLength: 4,
                    onChanged: (val){
                      if(val.length==4){
                        setState(() {
                          FocusScope.of(context).requestFocus(myFocusNode4);

                        });
                      }
                      if(val.length == 0){
                        FocusScope.of(context).requestFocus(myFocusNode2);
                      }
                    },
                  decoration: InputDecoration(
                    counterText: '',
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: widget.hint==null ? "****": widget.hint,
                    hintStyle: styles.hintsStyle(),
                  ),
                  style: TextStyle(
                      fontSize: widget.fontSize ?? 16,
                      fontFamily: "RobotoLight",
                      color: Colors.black
                  ),
                  focusNode:  myFocusNode3,
                )),
                SizedBox(width: size.convertWidth(context, 6),),
                Container(width : size.convertWidth(context, 40),child: TextFormField(
                  maxLength: 4,
                  onChanged: (val){
                    if(val.length==4){
                      setState(() {
                        FocusScope.of(context).requestFocus(myFocusNode5);

                      });
                    }
                    if(val.length == 0){
                      FocusScope.of(context).requestFocus(myFocusNode3);
                    }
                  },
                  decoration: InputDecoration(
                    counterText: '',
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: widget.hint==null ? "****": widget.hint,
                    hintStyle: styles.hintsStyle(),
                  ),
                  style: TextStyle(
                      fontSize: widget.fontSize ?? 16,
                      fontFamily: "RobotoLight",
                      color: Colors.black
                  ),
                  focusNode:  myFocusNode4,
                )),
                SizedBox(width: size.convertWidth(context, 6),),
                Container(width : size.convertWidth(context, 40),child: TextFormField(
                  maxLength: 4,
                  onChanged: (val){
                    if(val.length==4){
                    }
                    if(val.length == 0){
                      FocusScope.of(context).requestFocus(myFocusNode4);
                    }
                  },
                  decoration: InputDecoration(
                    counterText: '',
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: widget.hint==null ? "1234": widget.hint,
                    hintStyle: styles.hintsStyle(),
                  ),
                  style: TextStyle(
                      fontSize: widget.fontSize ?? 16,
                      fontFamily: "RobotoLight",
                      color: Colors.black
                  ),
                  focusNode:  myFocusNode5,
                )),
              ],),
              Image.asset("assets/icons/mastercard.png")
            ],
          ),
        ),
      );
  }
}
