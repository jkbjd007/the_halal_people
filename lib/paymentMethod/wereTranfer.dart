import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
class wereTransfer extends StatefulWidget {
  @override
  _wereTransferState createState() => _wereTransferState();
}

class _wereTransferState extends State<wereTransfer> {
  bool radio1 = true;
  bool radio2 = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset("assets/icons/wereTranfer.png"),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 80)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text:"Add Your Bank Account Information",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
           SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
              child: Row(
                children: <Widget>[
                  Text("Name of Beneficiary",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              child: customTextField(
                textFieldWidth: size.convertWidth(context, 301),
                textFieldHeight: size.convert(context, 58),
                isPadding: true,
                hints: "George Backer",
                radius: 10,
              ),
            ),

            SizedBox(height: size.convert(context, 15),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
              child: Row(
                children: <Widget>[
                  Text("Bank Name",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              child: customTextField(
                textFieldWidth: size.convertWidth(context, 301),
                textFieldHeight: size.convert(context, 58),
                isPadding: true,
                hints: "TBC Bank",
                radius: 10,
              ),
            ),

            SizedBox(height: size.convert(context, 15),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
              child: Row(
                children: <Widget>[
                  Text("Account Number (IBAN)",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              child: customTextField(
                textFieldWidth: size.convertWidth(context, 301),
                textFieldHeight: size.convert(context, 58),
                isPadding: true,
                hints: "6000000001SDFSG45345",
                radius: 10,
              ),
            ),

            SizedBox(height: size.convert(context, 15),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
              child: Row(
                children: <Widget>[
                  Text("SWIFT",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                ],
              ),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              child: customTextField(
                textFieldWidth: size.convertWidth(context, 301),
                textFieldHeight: size.convert(context, 58),
                isPadding: true,
                hints: "TBC33GEO",
                radius: 10,
              ),
            ),
            SizedBox(height: size.convert(context, 15),),
            swapButton(
              onClick: (){
                Navigator.pop(context);
              },
               buttonWidth: size.convertWidth(context, 301),
              buttonHieght: size.convert(context, 55),
              textColor: Colors.white,
              buttonText: "SAVE",
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              buttonColor: Colors.black,
            ),

          ],),),);
  }
}
