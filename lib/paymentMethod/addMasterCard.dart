import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/paymentMethod/creditCardField.dart';
import 'package:the_halal_people/paymentMethod/creditCardSettings.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/string.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/sign/otpVerification.dart';
class addMasterCard extends StatefulWidget {
  @override
  _addMasterCardState createState() => _addMasterCardState();
}

class _addMasterCardState extends State<addMasterCard> {
 String selectCard;
 String selectyear;
 String selectmonth;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  List <String> withDraw = ["master","paypal"];
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(height: size.convert(context, 20),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convert(context, 36)),
            child: Row(children: <Widget>[
              Text("Add New Card Info",style: styles.pageTitleStyle(fontSize: size.convert(context, 25)),),
            ],),),
          SizedBox(height: size.convert(context, 5),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Drive license number is needed if driver has registered a car. For bicycle it is not necessary.",
                            style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                          )
                        ]
                    ),),
                )
              ],
            ),
          ),
          SizedBox(height: size.convert(context, 20),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
            child: Row(
              children: <Widget>[
                Text("Card Type",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
              ],
            ),),
          SizedBox(height: size.convert(context, 3),),
          DottedBorder(
            color:  Colors.grey.withOpacity(0.5),
            strokeWidth: 1,
            dashPattern: [8, 4],
            radius: Radius.circular(10),
            strokeCap: StrokeCap.round,
            borderType: BorderType.RRect,
            child: Container(
              width: size.convertWidth(context, 301),
              height: size.convert(context, 55),
              decoration: BoxDecoration(
                  color: textFieldBgColor,
                  borderRadius: BorderRadius.circular(10)
              ),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 27),
                ),
                child: Center(
                  child: DropdownButton(
                    underline: Container(),
                    style: styles.hintsStyle(),
//              focusColor: Colors.blue,
                    isExpanded: true,
                    icon: Icon(Icons.keyboard_arrow_down,color: hintsColor,),
                    hint: Text("Select Card",style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                    // Not necessary for Option 1
                    value: selectCard,
                    onChanged: (newValue) {
                      setState(() {
                        selectCard = newValue;
                      });
                    },

                    items: withDraw.map((title) {
                      return DropdownMenuItem(
                        child: Row(
                          children: <Widget>[
                            Image.asset(title=="master"? "assets/icons/mastercard.png" :"assets/icons/icon_paypal.png"),
                            SizedBox(width: size.convertWidth(context, 15),),
                            Text(title,style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                          ],
                        ),
                        value: title,
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: size.convert(context, 10),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
            child: Row(
              children: <Widget>[
                Text("Card Number",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
              ],
            ),),
          SizedBox(height: size.convert(context, 3),),
          Container(
            color: textFieldBgColor,
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 50)),
              child:creditCardField()),
          SizedBox(height: size.convert(context, 30),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 45)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Text("Expiration Date",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                      ],
                    ),),
                  Container(child: Row(children: <Widget>[
                    DottedBorder(
                      color:  Colors.grey.withOpacity(0.5),
                      strokeWidth: 1,
                      dashPattern: [8, 4],
                      radius: Radius.circular(10),
                      strokeCap: StrokeCap.round,
                      borderType: BorderType.RRect,
                      child: Container(
                        width: size.convertWidth(context, 85),
                        height: size.convert(context, 58),
                        decoration: BoxDecoration(
                            color: textFieldBgColor,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
                          ),
                          child: Center(
                            child: DropdownButton(
                              underline: Container(),
                              style: styles.hintsStyle(),
//              focusColor: Colors.blue,
                              isExpanded: true,
                              icon: Icon(Icons.keyboard_arrow_down,color: hintsColor,),
                              hint: Text("2020",style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                              // Not necessary for Option 1
                              value: selectyear,
                              onChanged: (newValue) {
                                setState(() {
                                  selectyear = newValue;
                                });
                              },

                              items: years.map((title) {
                                return DropdownMenuItem(
                                  child: Text(title,style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                                  value: title,
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: size.convertWidth(context, 7),),
                    DottedBorder(
                      color:  Colors.grey.withOpacity(0.5),
                      strokeWidth: 1,
                      dashPattern: [8, 4],
                      radius: Radius.circular(10),
                      strokeCap: StrokeCap.round,
                      borderType: BorderType.RRect,
                      child: Container(
                        width: size.convertWidth(context, 65),
                        height: size.convert(context, 58),
                        decoration: BoxDecoration(
                            color: textFieldBgColor,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 10),
                          ),
                          child: Center(
                            child: DropdownButton(
                              underline: Container(),
                              style: styles.hintsStyle(),
//              focusColor: Colors.blue,
                              isExpanded: true,
                              icon: Icon(Icons.keyboard_arrow_down,color: hintsColor,),
                              hint: Text("01",style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                              // Not necessary for Option 1
                              value: selectmonth,
                              onChanged: (newValue) {
                                setState(() {
                                  selectmonth = newValue;
                                });
                              },

                              items: months.map((title) {
                                return DropdownMenuItem(
                                  child: Text(title,style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),),
                                  value: title,
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],),)
                ],),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Text("CVV/CVC",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
                        ],
                      ),),
                    Container(
                      child: DottedBorder(
                        color:  Colors.grey.withOpacity(0.5),
                        strokeWidth: 1,
                        dashPattern: [8, 4],
                        radius: Radius.circular(10),
                        strokeCap: StrokeCap.round,
                        borderType: BorderType.RRect,
                        child: Container(
                          color: textFieldBgColor,
                          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 13)),
                          width: size.convertWidth(context, 68),
                          height: size.convert(context, 58),
                          child: Center(
                            child: TextFormField(
                            maxLength: 3,
                            onChanged: (val){
                            },
                            decoration: InputDecoration(
                              counterText: '',
                              disabledBorder: InputBorder.none,
                              border: InputBorder.none,
                              hintText: "123",
                              hintStyle:  styles.pageTitleStyle(fontSize: size.convert(context, 16)),
                            ),
                            style:  styles.pageTitleStyle(fontSize: size.convert(context, 16)),

                        ),
                          ),),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: size.convert(context, 20),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 55)),
            child: Row(
              children: <Widget>[
                Text("George Backer",style: styles.slideparaStyle(fontSize: size.convert(context, 12)),),
              ],
            ),),
          Container(
            
            child: customTextField(
              textFieldWidth: size.convertWidth(context, 301),
            textFieldHeight: size.convert(context, 58),
            radius: 10,
              isPadding: true,
              hints: "George Backer",
          ),),
          SizedBox(height: size.convert(context, 20),),
          swapButton(
            onClick: (){
              Navigator.pop(context, PageTransition(child: creditCardSettings(),type: PageTransitionType.leftToRight));
            },
            buttonWidth: size.convertWidth(context, 301),
            buttonHieght: size.convert(context, 58),
            buttonColor: Colors.black,
            circleBorderColor: Colors.transparent,
            circleColor: Colors.transparent,
            buttonText: "SAVE",
            textColor: Colors.white,
          )
        ],),
      ),

    );
  }
}