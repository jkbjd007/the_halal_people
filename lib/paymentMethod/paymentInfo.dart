import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/paymentMethod/payMethod.dart';
import 'package:the_halal_people/paymentMethod/withDrawHistory.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/profileInfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
class paymentInfo extends DrawerContent {
  @override
  _paymentInfoState createState() => _paymentInfoState();
}

class _paymentInfoState extends State<paymentInfo> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  CarouselController _controller = CarouselController();
  int selectedBody = 3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
          trailingIcon: InkWell(
              onTap: widget.onMenuPressed,
              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Profile", style: styles.pageTitleStyle(),),
          leadingIcon: Image.asset("assets/icons/rightArrowIcon.png"),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/myWallet.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("My Wallet",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Enter your new password and then click on the “Save” button below.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: withDrawHistory(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/withDrawPayment.png"),
                  buttonText: "   Withdraw History",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
            SizedBox(height: size.convert(context, 10),),
            Container(

                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: payMethod(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/addPayment.png"),
                  buttonText: "  Add Payout Methods",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
          ],),
      ),
    );
  }
}