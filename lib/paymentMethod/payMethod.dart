import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/drawar/drawer.dart';
import 'package:the_halal_people/paymentMethod/creditCardSettings.dart';
import 'package:the_halal_people/paymentMethod/paypalSetting.dart';
import 'package:the_halal_people/paymentMethod/wereTranfer.dart';
import 'package:the_halal_people/paymentMethod/withDrawHistory.dart';
import 'package:the_halal_people/profile/addCompanyInfo.dart';
import 'package:the_halal_people/profile/profileInfo.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/dotedButton.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
class payMethod extends DrawerContent {
  @override
  _payMethodState createState() => _payMethodState();
}

class _payMethodState extends State<payMethod> {
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  CarouselController _controller = CarouselController();
  int selectedBody = 3;
  bool paypalEnable = false;
  bool masterEnable = true;
  bool wereTranferEnable = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Payment Methods", style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: size.convert(context, 30),),
            Image.asset("assets/icons/payMethod.png"),
            SizedBox(height: size.convert(context, 10),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Payment Methods",style: styles.pageTitleStyle(fontSize: size.convert(context, 20)),),
              ],
            ),
            SizedBox(height: size.convert(context, 10),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Enter your new password and then click on the “Save” button below.",
                              style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                            )
                          ]
                      ),),
                  )
                ],
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(children: <Widget>[
                radioButton(
                  onchange: (val){
                    setState(() {
                      if(val){
                        masterEnable =val;
                        paypalEnable = false;
                        wereTranferEnable = false;
                      }
                      else{
                        paypalEnable = val;
                        paypalEnable = true;
                        wereTranferEnable = false;
                      }
                    });
                  },
                  enable: masterEnable,
                ),
                SizedBox(width: size.convertWidth(context, 15),),
                Text("SET AS PRIMARY",style: styles.slideparaStyle(fontSize: size.convert(context, 13)),)
              ],),),
            SizedBox(height: size.convert(context, 4),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: creditCardSettings(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/mastercard.png"),
                  buttonText: "   Credit Card",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(children: <Widget>[
               radioButton(
                  onchange: (val){
                    setState(() {
                      if(val){
                        paypalEnable =val;
                        masterEnable = false;
                        wereTranferEnable = false;
                      }
                      else{
                        paypalEnable = val;
                        masterEnable = true;
                        wereTranferEnable = false;
                      }
                    });
                  },
                  enable: paypalEnable,
                ),
                SizedBox(width: size.convertWidth(context, 15),),
                Text("SET AS PRIMARY",style: styles.slideparaStyle(fontSize: size.convert(context, 13)),)
            ],),),
            SizedBox(height: size.convert(context, 4),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                     Navigator.push(context, PageTransition(child: paypalSetting(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/icon_paypal.png"),
                  buttonText: "  PayPal",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),

            SizedBox(height: size.convert(context, 20),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(children: <Widget>[
                radioButton(
                  onchange: (val){
                   setState(() {
                     if(val){
                       wereTranferEnable =val;
                       masterEnable = false;
                       paypalEnable = false;
                     }
                     else{
                       wereTranferEnable = val;
                       masterEnable = true;
                       paypalEnable = false;
                     }
                   });
                  },
                  enable: wereTranferEnable,
                ),
                SizedBox(width: size.convertWidth(context, 15),),
                Text("SET AS PRIMARY",style: styles.slideparaStyle(fontSize: size.convert(context, 13)),)
              ],),),
            SizedBox(height: size.convert(context, 4),),
            Container(
                margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
                child: dotedButton(
                  onClick: (){
                    Navigator.push(context, PageTransition(child: wereTransfer(),type: PageTransitionType.rightToLeft));
                  },
                  buttonHeight: size.convert(context, 64),
                  leadingIcon: Image.asset("assets/icons/weretransfer.png"),
                  buttonText: "   Were Transfer",
                  tralingIcon: Image.asset("assets/icons/buttonArrow.png"),
                )),
          ],),
      ),
    );
  }
}