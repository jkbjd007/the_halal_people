import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/paymentMethod/addMasterCard.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/radioButton.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/sign/otpVerification.dart';
class creditCardSettings extends StatefulWidget {
  @override
  _creditCardSettingsState createState() => _creditCardSettingsState();
}

class _creditCardSettingsState extends State<creditCardSettings> {
  bool pasTextDis = false;
  bool rePasTextDis = false;
  bool masterEnable1 = true;
  bool masterEnable2 = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  List withDraw = [{"icon":"assets/icons/mastercard.png","date":"15/01/2020","orderId":"886402","price":"\$345.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"15/01/2020","orderId":"886402","price":"\$3145.90"},
    {"icon":"assets/icons/bank.png","date":"15/01/2020","orderId":"886402","price":"\$45.90"},
    {"icon":"assets/icons/mastercard.png","date":"15/01/2020","orderId":"886402","price":"\$345.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"15/01/2020","orderId":"886402","price":"\$3453.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"15/01/2020","orderId":"886402","price":"\$3435.90"},
    {"icon":"assets/icons/mastercard.png","date":"15/01/2020","orderId":"886402","price":"\$345.90"},
  ];
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(children: <Widget>[
        SizedBox(height: size.convert(context, 20),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convert(context, 36)),
            child: Row(children: <Widget>[
            Text("Credit Cart Settings",style: styles.pageTitleStyle(fontSize: size.convert(context, 25)),),
          ],),),
          SizedBox(height: size.convert(context, 5),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertHeight(context, 40)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Drive license number is needed if driver has registered a car. For bicycle it is not necessary.",
                            style: styles.slideparaStyle(fontSize: size.convert(context, 13)),
                          )
                        ]
                    ),),
                )
              ],
            ),
          ),
          SizedBox(height: size.convert(context, 20),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
            child: Row(children: <Widget>[
              radioButton(
                onchange: (val){
                  setState(() {
                    masterEnable1 = !masterEnable1;
                    masterEnable2 = !masterEnable2;
                  });
                },
                enable: masterEnable1,
              ),
              SizedBox(width: size.convertWidth(context, 15),),
              Text("SET AS PRIMARY",style: styles.slideparaStyle(fontSize: size.convert(context, 13)),)
            ],),),
          SizedBox(height: size.convert(context, 4),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
            child: Image.asset("assets/icons/ing-mastercard-creditcard.png"),),
          SizedBox(height: size.convert(context, 20),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 60)),
            child: Row(children: <Widget>[
              radioButton(
                onchange: (val){
                  setState(() {
                    masterEnable1 = !masterEnable1;
                    masterEnable2 = !masterEnable2;
                  });
                },
                enable: masterEnable2,
              ),
              SizedBox(width: size.convertWidth(context, 15),),
              Text("SET AS PRIMARY",style: styles.slideparaStyle(fontSize: size.convert(context, 13)),)
            ],),),
          SizedBox(height: size.convert(context, 4),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
            child: Image.asset("assets/icons/download.png"),),
          SizedBox(height: size.convert(context, 20),),
          InkWell(
            onTap: (){
                    Navigator.push(context, PageTransition(child: addMasterCard(),
                    type: PageTransitionType.upToDown));
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: DottedBorder(
                color:  Colors.grey,
                strokeWidth: 1,
                dashPattern: [8, 4],
                radius: Radius.circular(50),
                strokeCap: StrokeCap.round,
                borderType: BorderType.RRect,
                child:Container(
                  height: size.convert(context, 55),
                  child: Center(child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
        Image.asset("assets/icons/plus.png"),
              SizedBox(width: size.convertWidth(context, 15),),
              Text("ADD NEW CARD",style: styles.pageTitleStyle(fontSize: size.convert(context, 16)),)
            ],),),
                ),),),
          ),
          SizedBox(height: size.convert(context, 10),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 90)),
            child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
            children: [
              TextSpan(
                text: "Paying by card is safe and convenient. You do not need to get a wallet and wait for the change: the money will be charged automatically after the trip",
                style: styles.slideparaStyle(fontSize: size.convert(context, 8))
              )
            ]
          ),),),
          SizedBox(height: size.convert(context, 20),),
        ],),
      ),

    );
  }
}