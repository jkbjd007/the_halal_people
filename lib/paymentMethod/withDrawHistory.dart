import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:the_halal_people/repeatedWidgeds/CustomAppBar.dart';
import 'package:the_halal_people/repeatedWidgeds/customTextField.dart';
import 'package:the_halal_people/repeatedWidgeds/swapButton.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/sign/otpVerification.dart';
class withDrawHistory extends StatefulWidget {
  @override
  _withDrawHistoryState createState() => _withDrawHistoryState();
}

class _withDrawHistoryState extends State<withDrawHistory> {
  bool pasTextDis = false;
  bool rePasTextDis = false;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  List withDraw = [{"icon":"assets/icons/mastercard.png","date":"15/01/2020","orderId":"886402","price":"\$345.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"15/01/2020","orderId":"886402","price":"\$3145.90"},
    {"icon":"assets/icons/bank.png","date":"15/01/2020","orderId":"886402","price":"\$45.90"},
    {"icon":"assets/icons/mastercard.png","date":"15/01/2020","orderId":"886402","price":"\$345.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"15/01/2020","orderId":"886402","price":"\$3453.90"},
    {"icon":"assets/icons/icon_paypal.png","date":"15/01/2020","orderId":"886402","price":"\$3435.90"},
    {"icon":"assets/icons/mastercard.png","date":"15/01/2020","orderId":"886402","price":"\$345.90"},
  ];
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 60)),
        child: CustomAppBar(
          hight: size.convert(context, 60),
          parentContext: context,
//          trailingIcon: InkWell(
//              onTap: widget.onMenuPressed,
//              child: Image.asset("assets/icons/menu.png")),
          centerWigets: Text("Withdraw History",style: styles.pageTitleStyle(),),
          leadingIcon: InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Image.asset("assets/icons/rightArrowIcon.png")),
        ),
      ),
      body: body(),
      //bottomNavigationBar: customAppBar(),
    );
  }
  body(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
          SizedBox(height: size.convert(context, 30),),
          Container(
            margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 34)),
            child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(children: <Widget>[
                Text("BALANCE",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                SizedBox(height: size.convert(context, 3),),
                Text("\$234.30",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 25),color: Color(0xff1308fe)),)
              ],),
              Column(children: <Widget>[
                Text("TOTAL WITHDRAW",style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
                SizedBox(height: size.convert(context, 3),),
                Text("\$33,346.50",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 25),color: Colors.black),)
              ],)
            ],
          ),),
          SizedBox(height: size.convert(context, 15),),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 43)),
                  child: Text("Add Amount"),
                ),
              ],
            ),
            SizedBox(height: size.convert(context, 3),),
            Container(
              width: size.convertWidth(context, 335),
              height: size.convert(context, 55),
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 34)),
              child: customTextField(
                isPadding: true,
                textInputType: TextInputType.number,
                radius: size.convert(context, 5),
                hints: "Add Amount",
                bgColor: Colors.white,
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            swapButton(
              buttonWidth: size.convertWidth(context, 335),
              buttonHieght
                  : size.convert(context, 55),
              buttonText: "WITHDRAW",
              buttonColor: Colors.black,
              textColor: Colors.white,
              circleColor: Colors.transparent,
              fontfamily: "RobotoCondensed-Light",
            ),
            SizedBox(height: size.convert(context, 30),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
              Text("WithDraw History",
              style: styles.slideHeadingStyle(fontSize: size.convert(context, 16)),),
              Text("View All",
                style: styles.slideHeadingStyle(fontSize: size.convert(context, 12)),),
            ],),),
            SizedBox(height: size.convert(context, 3),),
            Container(
              margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 40)),
              child: DottedBorder(
                color:  Colors.grey,
                strokeWidth: 1,
                dashPattern: [8, 4],
                radius: Radius.circular(10),
                strokeCap: StrokeCap.round,
                borderType: BorderType.RRect,
                child: ListView.separated(
                  physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index){
                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20),vertical: size.convert(context, 20)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                        Row(
                          children: <Widget>[
                            Image.asset(withDraw[index]["icon"]),
                            SizedBox(width: size.convertWidth(context, 20),),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                              Text(withDraw[index]["date"]??"",
                              style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),),),
                              Text("Order #${withDraw[index]["orderId"]??""}",
                              style: styles.fontRegular(fontSize: size.convert(context, 12),color: Color(0xffaab2b7)),)
                            ],)
                          ],
                        ),
                        Container(child: Text(withDraw[index]["price"]??"",style: styles.RobotoCondensedRegular(fontSize: size.convert(context, 16),color: Color(0xff1308fe)),),)
                      ],),);
                    },
                    separatorBuilder: (BuildContext context, int index){
                      return Divider(

                      );
                    },
                    itemCount: withDraw.length ?? 0),
              ),
            ),
            SizedBox(height: size.convert(context, 20),),
            swapButton(
              buttonWidth: size.convertWidth(context, 335),
              buttonHieght
                  : size.convert(context, 55),
              buttonText: "LOAD MORE",
              buttonColor: Color(0xff1308fe),
              textColor: Colors.white,
              circleBorderColor: Colors.transparent,
              circleColor: Colors.transparent,
              fontfamily: "RobotoCondensed-Regular",
            ),
            SizedBox(height: size.convert(context, 20),),
        ],),
      ),

    );
  }
}