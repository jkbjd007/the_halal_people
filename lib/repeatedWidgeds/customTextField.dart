

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/size.dart';
class customTextField extends StatelessWidget {
  double textFieldWidth;
  bool mutiLine;
  bool isPadding;
  double textFieldHeight;
  double radius;
  double fontSize;
  Widget icons;
  Widget nameIcons;
  bool isWidth;
  bool istralingIcon;
  Widget tralingIcon;
  bool isEnable;
  bool obscureText;
  String hints;
  String fontFamily;
  Color BorderColor;
  Color bgColor;
  Color fontColor;
  TextInputType textInputType;
  TextEditingController textEditingController;
  customTextField({this.textFieldHeight,this.fontFamily,this.fontSize,this.hints,this.textEditingController,this.BorderColor,this.obscureText,this.bgColor,this.fontColor,this.icons,this.isEnable,this.radius,this.textFieldWidth,this.textInputType,this.mutiLine = false,this.nameIcons,this.isWidth = false,this.tralingIcon,this.istralingIcon = false,this.isPadding=false});

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      color: BorderColor ?? Colors.grey,
      strokeWidth: 1,
      dashPattern: [8, 4],
      radius: Radius.circular(radius??50),
      strokeCap: StrokeCap.round,
      borderType: BorderType.RRect,
      child: Container(
        padding: isPadding ? EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20)) : EdgeInsets.symmetric(horizontal: 0),
        width: textFieldWidth ?? 301,
        height: textFieldHeight ?? 58,
        decoration: BoxDecoration(
        color: bgColor??textFieldBgColor,
          borderRadius: BorderRadius.circular(radius??50)
        ),
        child: mutiLine ? Container(
          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20)),
          child: TextFormField(
            maxLines: 10,
            enabled: isEnable,
            obscureText: obscureText ?? false,
            keyboardType: textInputType == null  ? TextInputType.text: textInputType,
            controller: textEditingController,
            style: TextStyle(
                fontSize: fontSize?? 16,
                fontFamily: "RobotoLight",
                color: Colors.black
            ),
            decoration: InputDecoration(
              disabledBorder: InputBorder.none,
              border: InputBorder.none,
              hintText: hints==null ? "": hints,
              hintStyle: styles.hintsStyle(),

            ),
          ),
        ) : Row(children: <Widget>[
          icons ?? Container(),
           Expanded(
             child: Container(
                child: Row(
                  children: <Widget>[
                    nameIcons ?? Container(),
                    isWidth ? SizedBox(width:  size.convertWidth(context,10),) : SizedBox(width:  0,),
                    Expanded(
                      child: TextField(
                      enabled: isEnable,
                      obscureText: obscureText ?? false,
                      keyboardType: textInputType == null  ? TextInputType.text: textInputType,
                      controller: textEditingController,
                      style: TextStyle(
                          fontSize: fontSize?? 16,
                          fontFamily: "RobotoLight",
                          color: Colors.black
                      ),
                      decoration: InputDecoration(
                          disabledBorder: InputBorder.none,
                          border: InputBorder.none,
                          hintText: hints==null ? "": hints,
                          hintStyle: styles.hintsStyle(),

                      ),
              ),
                    ),
                  ],
                ),),
           ),
          istralingIcon ? Container(margin: EdgeInsets.symmetric(horizontal: 20),
              child: tralingIcon ?? Container(width: 0,)) : Container(width: 0,)
        ],),
      ),
    ) ;
  }
}
class searchTextField extends StatelessWidget {
  double textFieldWidth;
  bool mutiLine;
  bool isPadding;
  double textFieldHeight;
  double radius;
  double fontSize;
  Widget icons;
  Widget nameIcons;
  bool isWidth;
  bool istralingIcon;
  Widget tralingIcon;
  bool isEnable;
  bool obscureText;
  String hints;
  String fontFamily;
  Color BorderColor;
  Color bgColor;
  Color fontColor;
  TextInputType textInputType;
  TextEditingController textEditingController;
  searchTextField({this.textFieldHeight,this.fontFamily,this.fontSize,this.hints,this.textEditingController,this.BorderColor,this.obscureText,this.bgColor,this.fontColor,this.icons,this.isEnable,this.radius,this.textFieldWidth,this.textInputType,this.mutiLine = false,this.nameIcons,this.isWidth = false,this.tralingIcon,this.istralingIcon = false,this.isPadding=false});

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      color: BorderColor ?? Colors.grey,
      strokeWidth: 1,
      dashPattern: [8, 4],
      radius: Radius.circular(radius??50),
      strokeCap: StrokeCap.round,
      borderType: BorderType.RRect,
      child: Container(
        padding: isPadding ? EdgeInsets.symmetric(horizontal: size.convertWidth(context, 5)) : EdgeInsets.symmetric(horizontal: 0),
        width: textFieldWidth ?? 301,
        height: textFieldHeight ?? 58,
        decoration: BoxDecoration(

        color: bgColor??textFieldBgColor,
          borderRadius: BorderRadius.circular(radius??50)
        ),
        child:  Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
          Expanded(
            child: TextField(
              enabled: isEnable,
              obscureText: obscureText ?? false,
              keyboardType: textInputType == null  ? TextInputType.text: textInputType,
              controller: textEditingController,
              style: TextStyle(
                  fontSize: fontSize?? 16,
                  fontFamily: "RobotoLight",
                  color: Colors.black
              ),
              decoration: InputDecoration(
                disabledBorder: InputBorder.none,
                border: InputBorder.none,
                hintText: hints==null ? "": hints,
                hintStyle: styles.hintsStyle(),

              ),
            ),
          ),
          istralingIcon ? Container(
              child: tralingIcon ?? Container(width: 0,)) : Container(width: 0,)
        ],),
      ),
    ) ;
  }
}
