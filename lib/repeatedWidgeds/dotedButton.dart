import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:the_halal_people/res/style.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/colors.dart';
class dotedButton extends StatelessWidget {
  Color BorderColor;
  Widget leadingIcon;
  Widget tralingIcon;
  String buttonText;
  double buttonHeight;
  Function onClick;
  dotedButton({this.BorderColor,this.buttonText,this.leadingIcon,this.tralingIcon,this.buttonHeight,this.onClick});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        if(onClick !=null) onClick();
      },
      child: DottedBorder(
        color: BorderColor ?? Colors.grey,
        strokeWidth: 1,
        dashPattern: [8, 4],
        radius: Radius.circular(10),
        strokeCap: StrokeCap.round,
        borderType: BorderType.RRect,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 20)),
          height: buttonHeight ?? 55,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
                color: textFieldBgColor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Row(children: <Widget>[
              leadingIcon ?? Container(),
              SizedBox(width: size.convertWidth(context, 5)),
              Text(buttonText ??"",style: styles.pageTitleStyle(fontSize: size.convert(context, 15)),),
            ],),
            tralingIcon ?? Container(),
          ],),
        ),
      ),
    );
  }
}
