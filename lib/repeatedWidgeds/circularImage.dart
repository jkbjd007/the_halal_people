import 'dart:io';


import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_svg/flutter_svg.dart';
import 'package:the_halal_people/res/colors.dart';

class circularImage extends StatelessWidget {
  String imageUrl;
  double h;
  double radius;
  double w;
  bool assetImage;
  bool isProduct;
  bool fileImage;
  File file;

  circularImage(
      {this.file,
      this.imageUrl,
      this.fileImage,
      this.h,
      this.w,
      this.assetImage = true,
        this.isProduct = false,
        this.radius
      });

  @override
  Widget build(BuildContext context) {
    return isProduct ? Container(
      height: h == null ? 24 : h,
      width: w == null ? 24 : w,
      decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(radius??0),
          image: imageUrl == null
              ? null
              : DecorationImage(
              image: fileImage??false
                  ? FileImage(file)
                  : assetImage
                  ? AssetImage(imageUrl)
                  : NetworkImage(imageUrl),
              fit: BoxFit.cover)),
    ) :
      DottedBorder(
      color:  Colors.grey,
      strokeWidth: 1,
      dashPattern: [20, 6],
      radius: Radius.circular(50),
      strokeCap: StrokeCap.round,
      borderType: BorderType.RRect,
      child: Container(
        height: h == null ? 24 : h,
        width: w == null ? 24 : w,
        decoration: BoxDecoration(
            color: textFieldBgColor,
            shape: BoxShape.circle,
            image: imageUrl == null
                ? null
                : DecorationImage(
                    image: fileImage??false
                        ? FileImage(file)
                        : assetImage
                            ? AssetImage(imageUrl)
                            : NetworkImage(imageUrl),
                    fit: BoxFit.cover)),
      ),
    );
  }
}

class circularAssetImage extends StatelessWidget {
  String imageUrl;
  double h;
  double w;

  circularAssetImage({this.imageUrl, this.h, this.w});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: h == null ? 24 : h,
      width: w == null ? 24 : w,
      decoration: BoxDecoration(
          color: buttonColor,
          shape: BoxShape.circle,
          image: imageUrl == null
              ? null
              : DecorationImage(
                  image: AssetImage(imageUrl), fit: BoxFit.cover)),
    );
  }
}
