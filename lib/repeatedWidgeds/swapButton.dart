
import 'package:flutter/material.dart';

class swapButton extends StatelessWidget {
  double buttonHieght;
  double buttonWidth;
  double circleWidth;
  double circleHieght;
  double borderWidth;
  double radius;
  double textSize;
  Color buttonColor;
  Color circleColor;
  Color borderColor;
  Color textColor;
  Color circleBorderColor;
  Widget icon;
  String buttonText;
  String fontfamily;
  Function onClick;

  swapButton({this.buttonWidth,this.buttonHieght,this.buttonText,this.borderWidth,this.borderColor,this.buttonColor,this.circleColor,this.icon,this.onClick,this.textColor,this.textSize,
    this.circleHieght, this.circleWidth,this.circleBorderColor,this.fontfamily,this.radius
  });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        if(onClick !=null) onClick();
      },
      child: Container(
        width: buttonWidth ?? 230,
        height: buttonHieght ?? 55,
        decoration: BoxDecoration(
          color: buttonColor ?? Colors.white,
          border: Border.all(
              color: borderColor ?? Colors.black,
              width: borderWidth ?? 1
          ),
          borderRadius: BorderRadius.circular(radius??50),
        ),
        child: Stack(children: <Widget>[
          Center(
            child: Text(buttonText ?? "" , style: TextStyle(
                fontSize: textSize ?? 16,
                color:  textColor ?? Colors.black,
                fontFamily: fontfamily ?? "RobotoLight"
            ),),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              width: circleWidth ?? 55,
              height: circleHieght ?? 55,
              child: Center(child: icon,) ?? Container(),
              decoration: BoxDecoration(
                color: circleColor ?? Colors.white,
                border: Border.all(
                    width: 1,
                    color: circleBorderColor ?? Colors.black
                ),
//              border: Border(
//                  left:BorderSide(
//                    width: 1,
//                    color: borderColor ?? Colors.black,
//                  ),
//                top:BorderSide(
//                  width: 1,
//                  color: borderColor ?? Colors.transparent,
//                ),
//                right:BorderSide(
//                  width: 1,
//                  color: borderColor ?? Colors.transparent,
//                ),
//                bottom:BorderSide(
//                  width: 1,
//                  color: borderColor ?? Colors.transparent,
//                ),
//              ),
                borderRadius: BorderRadius.circular(50),
              ),
            ),)
        ],)
    ),);
  }
}
