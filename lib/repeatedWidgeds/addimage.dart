import 'dart:io';


import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_svg/flutter_svg.dart';
import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:the_halal_people/res/style.dart';

class addImage extends StatelessWidget {
  String imageUrl;
  double h;
  double w;
  bool assetImage;
  bool fileImage;
  File file;
  Color BorderColor;

  addImage(
      {this.file,
        this.imageUrl,
        this.fileImage,
        this.h,
        this.w,
        this.assetImage = true,
      this.BorderColor});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomRight,
      children: <Widget>[
        DottedBorder(
          color: BorderColor ?? Colors.black,
          strokeWidth: 1,
          dashPattern: [12, 6],
          radius: Radius.circular(500),
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5,vertical: 5),
            height: h == null ? 24 : h,
            width: w == null ? 24 : w,
            decoration: BoxDecoration(
                color: Color(0xffDFDFDF),
                shape: BoxShape.circle,
                image: imageUrl == null
                    ? null
                    : DecorationImage(
                    image: fileImage??false
                        ? FileImage(file)
                        : assetImage
                        ? AssetImage(imageUrl)
                        : NetworkImage(imageUrl),
                    fit: BoxFit.cover)),
            child: (imageUrl == null && file == null) ? Center(child:Text("LOGO",
            style: styles.pageTitleStyle(fontSize: size.convert(context, 15)),)) : null,
          ),
        ),
        Align(
          child: DottedBorder(
        color: BorderColor ?? Colors.black,
    strokeWidth: 1,
    dashPattern: [12, 6],
    radius: Radius.circular(500),
    strokeCap: StrokeCap.round,
    borderType: BorderType.RRect,
          child: Container(
            width: size.convert(context, 32),
            height: size.convert(context, 32),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(500),
            ),
            child: Image.asset("assets/icons/camera.png"),
          ),
        ),)
      ],
    );
  }
}

class circularAssetImage extends StatelessWidget {
  String imageUrl;
  double h;
  double w;

  circularAssetImage({this.imageUrl, this.h, this.w});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: h == null ? 24 : h,
      width: w == null ? 24 : w,
      decoration: BoxDecoration(
          color: buttonColor,
          shape: BoxShape.circle,
          image: imageUrl == null
              ? null
              : DecorationImage(
              image: AssetImage(imageUrl), fit: BoxFit.cover)),
    );
  }
}