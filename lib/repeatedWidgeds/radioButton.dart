import 'package:the_halal_people/res/colors.dart';
import 'package:the_halal_people/res/size.dart';
import 'package:flutter/material.dart';
class radioButton extends StatefulWidget {
  Function onchange;
  bool enable ;
  radioButton({this.onchange,this.enable});
  @override
  _radioButtonState createState() => _radioButtonState();
}

class _radioButtonState extends State<radioButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: <Widget>[
         Row(
          children: <Widget>[
            InkWell(
              onTap: (){
                setState(() {
                  widget.enable = !widget.enable;
                  widget.onchange(widget.enable);
                });
              },
              child: Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                        width: 1,
                        color:  Color(0xffc7c7cd)
                    )
                ),
                child: widget.enable ? Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: buttonColor,
                      border: Border.all(
                          width: 4,
                          color: Colors.white
                      )
                  ),
                ) : Container(width: 0.1,height: 0.1,),
              ),
            ),
          ],
        ),
      ],),
    );
  }
}
